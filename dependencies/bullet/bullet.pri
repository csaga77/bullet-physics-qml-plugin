BULLET_PATH=$(BULLET_PATH_ENVIRONMENT)

CONFIG += link_pkgconfig
PKGCONFIG += bullet

macx {
    BULLET_LIB_PATH=$${BULLET_PATH}/bin
    INCLUDEPATH += $${BULLET_PATH}/src
    LIBS += \
        -L"$${BULLET_LIB_PATH}"

    CONFIG(release, debug|release) {
        #release
        LIBS += \
            -lBulletDynamics \
            -lBulletCollision \
            -lLinearMath

    } else {
        #debug
        LIBS += \
            -lBulletDynamics_xcode4_x64_debug \
            -lBulletCollision_xcode4_x64_debug \
            -lLinearMath_xcode4_x64_debug
    }

}

windows {
    INCLUDEPATH += $${BULLET_PATH}/src
    CONFIG(release, debug|release) {
        BULLET_LIB_PATH=$${BULLET_PATH}/lib/release
    } else {
        BULLET_LIB_PATH=$${BULLET_PATH}/lib/debug
    }

    LIBS += \
        -L"$${BULLET_LIB_PATH}"

    CONFIG(release, debug|release) {
        #release
        LIBS += \
            -lBulletDynamics \
            -lBulletCollision \
            -lLinearMath

    } else {
        #debug
        LIBS += \
            -lBulletDynamics_debug \
            -lBulletCollision_debug \
            -lLinearMath_debug
    }

}

ios {
    BULLET_LIB_PATH=$${BULLET_PATH}/bin
    INCLUDEPATH += $${BULLET_PATH}/src
    LIBS += \
        -L"$${BULLET_LIB_PATH}"

    CONFIG(release, debug|release) {
        #release
        LIBS += \
            -lBulletDynamics_xcode4_x64_release \
            -lBulletCollision_xcode4_x64_release \
            -lLinearMath_xcode4_x64_release

    } else {
        #debug
        LIBS += \
            -lBulletDynamics_xcode4_x64_debug \
            -lBulletCollision_xcode4_x64_debug \
            -lLinearMath_xcode4_x64_debug
    }
}

android {

    BULLET_LIB_PATH=$${BULLET_PATH}/lib
    INCLUDEPATH += $${BULLET_PATH}/include/bullet
    LIBS += \
        -L"$${BULLET_LIB_PATH}"

    CONFIG(release, debug|release) {
        #release
        LIBS += \
            -L"$${BULLET_LIB_PATH}/release"

    } else {
        #debug
        LIBS += \
            -L"$${BULLET_LIB_PATH}/debug"
    }

    LIBS += \
        -lBulletDynamics \
        -lBulletCollision \
        -lLinearMath

}


isEmpty(BULLET_PATH):error("bullet.pri: Bullet depdendency not found. Re-run update dependencies script.")


