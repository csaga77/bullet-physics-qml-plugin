/*!
Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
import Qt3D.Core 2.0
import Qt3D.Render 2.9
import Qt3D.Input 2.0
import Qt3D.Extras 2.9
import Qt3D.Logic 2.0

import QtQml.Models 2.2
import QtQuick 2.10 as QQ2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Scene3D 2.0
import QBullet 1.0 as QB
import QBullet.Tools 1.0 as QBTools
import QRender 1.0 as QRender
import "BulletToolsQMLPlugin/utils.js" as Utils

QQ2.Item {
    id: root
    focus: true
    property bool showDebug: false
    property real pickingIndicatorSize: 0.2
    property bool followVehicle: false
    property real substractRadius: 1
    property real gamma: 1.8
    property alias exposure: viewCamera.exposure

    QQ2.Keys.onPressed: {
        if(event.key===Qt.Key_Up) {
            vehicle.forward();
        } else if(event.key===Qt.Key_Down) {
            vehicle.backward();
        } else if(event.key===Qt.Key_Left) {
            vehicle.turnLeft();
        } else if(event.key===Qt.Key_Right) {
            vehicle.turnRight();
        }
    }

    QQ2.Keys.onReleased: {

        if(event.key===Qt.Key_Up) {
            vehicle.stop();
        } else if(event.key===Qt.Key_Down) {
            vehicle.stop();
        } else if(event.key===Qt.Key_Left) {
            vehicle.stopTurning();
        } else if(event.key===Qt.Key_Right) {
            vehicle.stopTurning();
        }
    }

    property alias world: world1

    Scene3D {
        id: scene3D
        anchors.fill: parent

        cameraAspectRatioMode: Scene3D.UserAspectRatio

        aspects: ["render", "input", "logic"]

        multisample: true // default

        Entity {
            id: sceneRoot

            Camera {
                id: viewCamera
                projectionType: CameraLens.PerspectiveProjection
                fieldOfView: 40
                aspectRatio: appWindow.width / appWindow.height
                nearPlane: 1
                farPlane: 200.0
                exposure: 0
                //                position: vehicle.origin.plus(Qt.vector3d(1, 1, 1).times(20))
                //                viewCenter: vehicle.origin
                position: Qt.vector3d(10, 10, 10)
                viewCenter: Qt.vector3d(0, 0, 0)
                upVector: Qt.vector3d(0, 1, 0)
                property vector3d lookAtVector: viewCenter.minus(position).normalized()
                property vector3d rightVector: lookAtVector.crossProduct(upVector).normalized()

                QQ2.Binding {
                    when: root.followVehicle
                    target: viewCamera; property: "position"; value: vehicle.origin.plus(Qt.vector3d(1, 1, 1).times(20))
                }

                QQ2.Binding {
                    when: root.followVehicle
                    target: viewCamera; property: "viewCenter"; value: vehicle.origin
                }
            }

            Camera {
                id: lightCamera

                property vector3d lightIntensity: Qt.vector3d(1,1,1)

                readonly property matrix4x4 shadowMatrix: Qt.matrix4x4(0.5, 0.0, 0.0, 0.5,
                                                                       0.0, 0.5, 0.0, 0.5,
                                                                       0.0, 0.0, 0.5, 0.5,
                                                                       0.0, 0.0, 0.0, 1.0)

                readonly property matrix4x4 shadowViewProjection: shadowMatrix.times(projectionMatrix.times(viewMatrix))

                position: vehicle.origin.plus(Qt.vector3d(30, 20, 30))
                viewCenter: vehicle.origin
                //                position: Qt.vector3d(30, 30, 30)
                //                viewCenter: Qt.vector3d(0, 0, 0)
                upVector: Qt.vector3d(0.0, 1.0, 0.0)


                //Use orthographic projection for direction light.
                projectionType: CameraLens.OrthographicProjection
                nearPlane: 1 // meters
                farPlane: 100 // meters
                //fieldOfView: 45
                //left, right, top, bottom define shadow map region.
                left: -20
                right: 20
                top: 20
                bottom: -20
                aspectRatio: 1
            }

            OrbitCameraController {
                enabled: !root.followVehicle
                camera: viewCamera
                linearSpeed: 100
                lookSpeed: 200
            }

            components: [
                RenderSettings {
                    QRender.ShadowFrameGraph {
                        id: frameGraph
                        clearColor: "#CC000000" // slightly transparent black
                        lightCamera: lightCamera
                        shadowMapSize: 2048
                        QRender.ShadowFrameGraphViewport {
                            camera: viewCamera
                            depthTexture: frameGraph.depthTexture
                            lightCamera: lightCamera
                            normalizedRect: Qt.rect(0, 0, 1, 1)
                            gamma: root.gamma
                        }
                    }
                },
                // Event Source will be set by the Qt3DQuickWindow
                InputSettings {}

            ]

            Entity {
                components: [
                    DirectionalLight {
                        color: "white" // default is white
                        intensity: 0.8 // default is 0.5
                        worldDirection: Qt.vector3d(-1, -1, -1);
                    },
                    EnvironmentLight {
                        enabled: parent.enabled

                        irradiance: TextureLoader {
                            source: "qrc:/assets/envmaps/cedar-bridge/cedar_bridge_irradiance.dds"
                            wrapMode {
                                x: WrapMode.ClampToEdge
                                y: WrapMode.ClampToEdge
                            }
                            generateMipMaps: false
                        }
                        specular: TextureLoader {
                            source: "qrc:/assets/envmaps/cedar-bridge/cedar_bridge_specular.dds"
                            wrapMode {
                                x: WrapMode.ClampToEdge
                                y: WrapMode.ClampToEdge
                            }
                            generateMipMaps: false
                        }
                    }
                ]
            }

            Car {
                id: vehicle
                world: world1
                showDebug: root.showDebug
            }

//            BulletTools.GroundPlane {
//                world: world1
//            }

            QBTools.Heightmap {
                id: heightmap
                world: world1
                heightmap: "qrc:/resources/Heightmap.png"
                scale: Qt.vector3d(0.5, 0.03, 0.5)
                friction: 0.95
            }

            QB.DiscreteDynamicsWorld {
                id: world1
                gravity: Qt.vector3d(0, -9.8, 0)
                running: true
            }

            QBTools.Picker {
                id: picker
                world: world1
                pickingIndicatorSize: root.pickingIndicatorSize

                onHitPositionChanged: {
                    //console.log(picker.hitBody);
                    if(picker.hasHit&&
                            picker.hitBody===heightmap.body) {
                        heightmap.substract(hitPosition, root.substractRadius);
                    }
                }
                onDragPositionChanged: {
                    if(picker.hasHit&&
                            picker.hitBody===heightmap.body) {
                        heightmap.substract(dragPosition, root.substractRadius);
                    }
                }

            }


        }//Entity: root entity

    }//Scene3D



    QQ2.MouseArea {
        anchors.fill: parent

        onPressed: {
            //console.log(mouse.y);
            var horz = (mouse.x - (root.width/2))*0.93;
            var vert = mouse.y - root.height/2;
            var tan = Utils.getTanFromDegrees(viewCamera.fieldOfView/2);
            var dist = (root.height*0.5)/tan;
            var c = viewCamera.position.plus(viewCamera.lookAtVector.times(dist));
            var upVector = viewCamera.rightVector.crossProduct(viewCamera.lookAtVector);

            var rayFrom = viewCamera.position;
            var rayTo = c.plus(viewCamera.rightVector.times(horz)).minus(upVector.times(vert));

            picker.pick(rayFrom, rayTo);
        }

        onPositionChanged: {
            var tan = Utils.getTanFromDegrees(viewCamera.fieldOfView/2);
            var dist = (root.height*0.5)/tan;
            var distHit = picker.hitVector.dotProduct(viewCamera.lookAtVector);
            var scale = distHit/dist;

            var horz = (mouse.x - (root.width/2))*0.93*scale;
            var vert = (mouse.y - root.height/2)*scale;
            var upVector = viewCamera.rightVector.crossProduct(viewCamera.lookAtVector);
            var c = viewCamera.position.plus(viewCamera.lookAtVector.times(distHit));
            var newPos = c.plus(viewCamera.rightVector.times(horz)).minus(upVector.times(vert));

            picker.dragTo(newPos);
        }

        onReleased: {
            picker.clear();
        }
    }

}
