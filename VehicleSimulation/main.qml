/*!
Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QRender 1.0 as QRender

ApplicationWindow {
    id: appWindow
    visible: true
    width: 1280
    height: 768

    VehicleSimulation {
        id: sceneRoot
        anchors.fill: parent
        showDebug: showDebug.checked
        followVehicle: followVehicle.checked
        substractRadius: substractRadius.value
        exposure: exposureSlider.value
        gamma: gammaSlider.value
    }

    QRender.FpsText {
        anchors {
            top: parent.top
            right: parent.right
        }
    }

    // can we have the application header only when there is no project page?
    header: ToolBar {
        id: toolBar
        onFocusChanged: {
            sceneRoot.focus = true;
        }

        RowLayout {
            spacing: 5 //default

            CheckBox {
                id: showDebug
                text: qsTr("Debug")
            }

            CheckBox {
                id: followVehicle
                text: qsTr("Follow")
                checked: true
            }
            ToolButton {
                text: sceneRoot.world.running?qsTr("Stop"):qsTr("Start")
                onClicked: {
                    sceneRoot.world.running = !sceneRoot.world.running;
                }
            }
            Text { text: "Dig radius" }
            Slider {
                id: substractRadius
                from: 0.1
                to: 5
            }
            Text { text: "Exposure" }
            Slider {
                id: exposureSlider
                Layout.fillWidth: true
                from: -2
                to: 2
            }
            Text { text: "Gamma" }
            Slider {
                id: gammaSlider
                Layout.fillWidth: true
                from: 1.2
                to: 2.8
                value: 1.8
            }
        }
    }

}
