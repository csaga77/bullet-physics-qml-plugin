/*!
BulletToolsQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
import Qt3D.Core 2.0
import Qt3D.Render 2.0

import QtQml.Models 2.2
import QBullet 1.0 as Bullet
import QRender 1.0 as QRender
import "utils.js" as Utils

Entity {
    id: root
    property Bullet.DiscreteDynamicsWorld world: null
    property real metalness: 0.5
    property real roughness: 0.5
    property real friction: 0.5
    property real rollingFriction: 0.5
    property real spinningFriction: 0.5
    property real restitution: 0.5

    function addBalls(numberOfBalls, radius, mass)
    {
        for (var i = 0; i < numberOfBalls; i++) {
            var data = {'x': 0,
                'y': 50 + radius,
                'z': 0,
                'radius': radius,
                'color': Utils.getRandomColor(),
                'restitution': 0.2,
                'mass': mass};
            balls.append(data);
        }
    }

    function addRandomBalls(numberOfBalls)
    {
        for (var i = 0; i < numberOfBalls; i++) {
            var radius = Utils.getRandomArbitrary(0.05, 10);
            var data = {'x': Utils.getRandomArbitrary(-30, 30),
                'y': Utils.getRandomArbitrary(radius, 100),
                'z': Utils.getRandomArbitrary(-30, 30),
                'radius': radius,
                'color': Utils.getRandomColor(),
                'restitution': Utils.getRandomArbitrary(0.1, 1),
                'mass': Utils.getRandomArbitrary(1, 20)};
            balls.append(data);
        }
    }

    function clearAll()
    {
        balls.clear();
    }

    ListModel {
        id: balls
    }

    NodeInstantiator {
        model: balls
        delegate: Entity {

            Bullet.SphereShape {
                id: sphereShape
                radius: model.radius
            }

            Bullet.RigidBody {
                id: ballBody
                origin: Qt.vector3d(model.x, model.y, model.z)
                world: root.world
                collisionShape: sphereShape
                mass: model.mass
                restitution: root.restitution
                friction: root.friction
                rollingFriction: root.rollingFriction
                spinningFriction: root.spinningFriction
                linearDamping: 0.5
                angularDamping: 0.5

            }

            QRender.Ball {
                matrix: ballBody.matrix
                radius: model.radius
                material: QRender.MetalRoughMaterial {
                    ambient: "#808080"
                    metalness: root.metalness
                    roughness: root.roughness
                    alpha: 1
                    textureScale: 1
                    diffuse: model.color

                    diffuseTexture: TextureLoader {
                        source: "qrc:/resources/earth_0.png"
                        generateMipMaps: true
                        minificationFilter: Texture.LinearMipMapLinear
                        magnificationFilter: Texture.Linear
                        maximumAnisotropy: 16.0
                        wrapMode {
                            x: WrapMode.Repeat
                            y: WrapMode.Repeat
                            z: WrapMode.Repeat
                        }
                    }

                }

            }
        }
    }


}
