/*!
Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
import Qt3D.Core 2.0
import Qt3D.Render 2.9
import Qt3D.Input 2.0
import Qt3D.Extras 2.9
import Qt3D.Logic 2.0

import QtQml.Models 2.2
import QtQuick 2.10 as QQ2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Scene3D 2.0

import QtSensors 5.0

import QBullet 1.0 as Bullet
import QBullet.Tools 1.0 as BulletTools
import QRender 1.0 as QRender
import "BulletToolsQMLPlugin/utils.js" as Utils

QQ2.Item {
    id: root
    focus: true
    property real pickingIndicatorSize: 1
    property bool useGravitySensor: false

    QQ2.Keys.onPressed: {
    }

    QQ2.Keys.onReleased: {
    }

    property alias ballGenerator: ballGenerator

    property alias world: world1

    Scene3D {
        id: scene3d
        anchors.fill: parent

        cameraAspectRatioMode: Scene3D.UserAspectRatio

        aspects: ["render", "input", "logic"]

        multisample: true // default

        Entity {
            id: sceneRoot

            Camera {
                id: viewCamera
                projectionType: CameraLens.PerspectiveProjection
                fieldOfView: 90
                aspectRatio: appWindow.width / appWindow.height
                nearPlane: 1
                farPlane: 500.0
                position: root.useGravitySensor?Qt.vector3d(0, 200, 0):Qt.vector3d(0, 100, 100)
                viewCenter: Qt.vector3d(0, 0, 0)
                upVector: Qt.vector3d(0, 1, 0)
                property vector3d lookAtVector: viewCenter.minus(position).normalized()
                property vector3d rightVector: lookAtVector.crossProduct(upVector).normalized()
                exposure: 0
            }

            Camera {
                id: lightCamera

                property vector3d lightIntensity: Qt.vector3d(1,1,1)

                readonly property matrix4x4 shadowMatrix: Qt.matrix4x4(0.5, 0.0, 0.0, 0.5,
                                                                       0.0, 0.5, 0.0, 0.5,
                                                                       0.0, 0.0, 0.5, 0.5,
                                                                       0.0, 0.0, 0.0, 1.0)

                readonly property matrix4x4 shadowViewProjection: shadowMatrix.times(projectionMatrix.times(viewMatrix))

                position: Qt.vector3d(150, 150, 150)
                viewCenter: Qt.vector3d(0.0, 0.0, 0.0)
                upVector: Qt.vector3d(0.0, 1.0, 0.0)

                property vector3d lookAtVector: viewCenter.minus(position).normalized()

                //Use orthographic projection for direction light.
                projectionType: CameraLens.OrthographicProjection
                nearPlane: 1 // meters
                farPlane: 500 // meters
                fieldOfView: 45
                //left, right, top, bottom define shadow map region.
                left: -150
                right: 150
                top: 150
                bottom: -150
                aspectRatio: 1
            }

            OrbitCameraController {
                camera: viewCamera
                linearSpeed: 100
                lookSpeed: 200
            }

            components: [
                RenderSettings {
                    QRender.ShadowFrameGraph {
                        id: frameGraph
                        clearColor: "#CC000000" // slightly transparent black
                        lightCamera: lightCamera
                        shadowMapSize: 2048
                        QRender.ShadowFrameGraphViewport {
                            camera: viewCamera
                            depthTexture: frameGraph.depthTexture
                            lightCamera: lightCamera
                            normalizedRect: Qt.rect(0, 0, 1, 1)
                        }
                    }
                },
                // Event Source will be set by the Qt3DQuickWindow
                InputSettings {}

            ]

            Entity {
                components: [
                    DirectionalLight {
                        color: "white" // default is white
                        intensity: 0.5 // default is 0.5
                        worldDirection: lightCamera.lookAtVector
                    },
                    EnvironmentLight {
                        enabled: parent.enabled
                        irradiance: TextureLoader {
                            source: "qrc:/assets/envmaps/cedar-bridge/cedar_bridge_irradiance.dds"
                            wrapMode {
                                x: WrapMode.ClampToEdge
                                y: WrapMode.ClampToEdge
                            }
                            generateMipMaps: false
                        }
                        specular: TextureLoader {
                            source: "qrc:/assets/envmaps/cedar-bridge/cedar_bridge_specular.dds"
                            wrapMode {
                                x: WrapMode.ClampToEdge
                                y: WrapMode.ClampToEdge
                            }
                            generateMipMaps: true
                        }
                    }
                ]
            }


            BulletTools.BallGenerator {
                id: ballGenerator
                world: world1
                metalness: 1.0
                roughness: 0
            }

            BulletTools.Sandbox {
                world: world1
                inContainer: false
                heightmap: "qrc:/resources/maze-01.png"
                enabledSpring: !root.useGravitySensor
                width: 250
                wallHeight: 30
                diamondSubdivision: false
                friction: 0.9
                restitution: 0.5
            }

            Accelerometer {
                id: accelerometer
                active: root.useGravitySensor
            }

            Bullet.DiscreteDynamicsWorld {
                id: world1
                gravity: (!root.useGravitySensor||accelerometer.reading===null)?
                             Qt.vector3d(0, -98, 0):
                             Qt.vector3d(accelerometer.reading.x,
                                         accelerometer.reading.z,
                                         -accelerometer.reading.y).times(-20);
            }

            //Indication of picking.
            BulletTools.Picker {
                id: picker
                world: world1
            }

        }//Entity: root entity

    }//Scene3D

    QQ2.MouseArea {
        anchors.fill: parent

        onPressed: {
            //console.log(mouse.y);
            var horz = (mouse.x - (root.width/2))*0.93;
            var vert = mouse.y - root.height/2;
            var tan = Utils.getTanFromDegrees(viewCamera.fieldOfView/2);
            var dist = (root.height*0.5)/tan;
            var c = viewCamera.position.plus(viewCamera.lookAtVector.times(dist));
            var upVector = viewCamera.rightVector.crossProduct(viewCamera.lookAtVector);

            var rayFrom = viewCamera.position;
            var rayTo = c.plus(viewCamera.rightVector.times(horz)).minus(upVector.times(vert));

            picker.pick(rayFrom, rayTo);
        }

        onPositionChanged: {
            var tan = Utils.getTanFromDegrees(viewCamera.fieldOfView/2);
            var dist = (root.height*0.5)/tan;
            var distHit = picker.hitVector.dotProduct(viewCamera.lookAtVector);
            var scale = distHit/dist;

            var horz = (mouse.x - (root.width/2))*0.93*scale;
            var vert = (mouse.y - root.height/2)*scale;
            var upVector = viewCamera.rightVector.crossProduct(viewCamera.lookAtVector);
            var c = viewCamera.position.plus(viewCamera.lookAtVector.times(distHit));
            var newPos = c.plus(viewCamera.rightVector.times(horz)).minus(upVector.times(vert));

            picker.dragTo(newPos);
        }

        onReleased: {
            picker.clear();
        }
    }
}
