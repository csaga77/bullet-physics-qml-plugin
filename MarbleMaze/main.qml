/*!
Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QRender 1.0 as QRender

ApplicationWindow {
    id: appWindow
    visible: true
    width: 1280
    height: 768

    MarbleMaze {
        id: scene
        anchors.fill: parent
        useGravitySensor: useGravitySensor.checked
    }

    QRender.FpsText {
        anchors {
            top: parent.top
            right: parent.right
        }
    }

    // can we have the application header only when there is no project page?
    header: ToolBar {
        id: toolBar

        RowLayout {
            spacing: 5 //default
            CheckBox {
                id: useGravitySensor
                text: qsTr("Gravity")
            }

            ToolButton {
                text: qsTr("Add")
                onClicked: {
                    scene.ballGenerator.addBalls(1, 5, 1);
                }
            }
            ToolButton {
                text: qsTr("Clear")
                onClicked: {
                    scene.ballGenerator.clearAll();
                }
            }
            ToolButton {
                text: scene.world.running?qsTr("Stop"):qsTr("Start")
                onClicked: {
                    scene.world.running = !scene.world.running;

                }
            }
        }
    }

}
