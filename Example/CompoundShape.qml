/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
import Qt3D.Core 2.0
import Qt3D.Extras 2.0
import Qt3D.Render 2.0
import QBullet 1.0 as QB
import QRender 1.0 as QRender

Entity {
    id: root
    property QB.DiscreteDynamicsWorld world: null

    QB.CompoundShape {
        id: compoundShape
        shapes: [
            QB.BoxShape {
                id: box1
                dimensions: Qt.vector3d(20, 10, 10)
                QB.Compound.position: Qt.vector3d(20, 0, 0)
            },
            QB.SphereShape {
                id: ball1
                radius: 10
                QB.Compound.position: Qt.vector3d(0, 0, 0)
            }
        ]
    }
    QB.RigidBody {
        id: compoundBody
        world: root.world
        origin: Qt.vector3d(0, 30, 0)
        collisionShape: compoundShape
        mass: 1
    }
    Transform {
        id: trans1
        matrix: compoundBody.matrix
    }

    Entity {
        components: [trans1]
        QRender.Box {
            dimensions: box1.dimensions
            matrix: box1.QB.Compound.matrix
            material: QRender.MetalRoughMaterial {
                ambient: "#808080"
                diffuse: "red"

            }
        }
        QRender.Ball {
            radius: ball1.radius
            position: ball1.QB.Compound.position
            rotation: ball1.QB.Compound.rotation
            material: QRender.MetalRoughMaterial {
                ambient: "#808080"
                alpha: 1
                diffuse: "green"
            }
        }
    }

}
