import Qt3D.Core 2.0
import Qt3D.Extras 2.0
import Qt3D.Render 2.0

Entity {
    id: root
    property alias origin: transform.translation
    property alias rotation: transform.rotation
    property alias matrix: transform.matrix
    property alias length: mesh.length
    property alias radius: mesh.radius
    property alias slices: mesh.slices
    property Material material

    CylinderMesh {
        id: mesh
    }

    Transform {
        id: transform
    }

    components: [
        mesh,
        transform,
        material
    ]

}
