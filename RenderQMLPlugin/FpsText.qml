import QtQuick.Controls 2.3 as Controls2
import QRender 1.0 as QRender

Controls2.Label {

    // If solely using QtQuick, you'd need something to change in the UI to trigger recalculation
    // e.g. a rotating rectangle. However if using a Scene3D within QtQuick, this isn't necessary.
    QRender.FPSMonitor {
        id: fpsMonitor
        currentWindow: appWindow // from root QML file's context
        enabled: true
    }

    color: "white"

    text: (fpsMonitor.averageFrequency | 0) + " FPS"// 0 decimal places
}
