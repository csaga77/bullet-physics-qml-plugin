import Qt3D.Core 2.0
import Qt3D.Render 2.10
import QtQml 2.2

Material {
    id: root

    property color ambient: "#808080"
    property Texture diffuseTexture: Texture2D { id: dummy }
    property color diffuse: Qt.rgba(1.0, 1.0, 1.0, 1.0)
    property color specular: Qt.rgba(0.01, 0.01, 0.01, 1.0)
    property real shininess: 150
    property alias textureScale: texTrans.scale
    property alias textureOffset: texTrans.translation
    property real alpha: 1
    readonly property bool hasTexture: diffuseTexture!==dummy&&diffuseTexture!==null
    property alias cullingMode: backFaceCulling.mode

    // optimisation - render aspect can ignore this component if the alpha is 0
    enabled: alpha > 0

    Transform {
        id: texTrans
    }

    parameters: [
        Parameter { name: "ambientColor"; value: ambient },
        Parameter { name: "diffuseTexture"; value: hasTexture?diffuseTexture:0 },
        Parameter { name: "diffuseColor"; value: diffuse },
        Parameter { name: "specularColor"; value: specular },
        Parameter { name: "shininess"; value: shininess },
        Parameter { name: "texMatrix"; value: texTrans.matrix },
        Parameter { name: "alphaValue"; value: alpha }
    ]

    effect: Effect {

        FilterKey {
            id: transparentRenderPassFilterKey
            name: "opacityType"
            value: alpha < 1 ? "semiTransparent" : "opaque"
        }

        FilterKey {
            id: shadowRenderPassFilterKey
            name: "pass"
            value: "shadowmap"
        }

        FilterKey {
            id: techniqueFilterKey
            name: "renderingStyle"
            value: "shadowRender"
        }

        ShaderProgram {
            id: gl3Shader

            vertexShaderCode: loadSource("qrc:/RenderQMLPlugin/shaders/gl3/default.vert")
            fragmentShaderCode: hasTexture?loadSource("qrc:/RenderQMLPlugin/shaders/gl3/diffusemap.frag")
                                          :loadSource("qrc:/RenderQMLPlugin/shaders/gl3/diffusemap_solidcolor.frag")
        }

        ShaderProgram {
            id: gl3ShadowShader
            vertexShaderCode: loadSource("qrc:/RenderQMLPlugin/shaders/gl3/shadowmap.vert")
            fragmentShaderCode: loadSource("qrc:/RenderQMLPlugin/shaders/gl3/shadowmap.frag")
        }

        ShaderProgram {
            id: es2Shader
            vertexShaderCode: loadSource("qrc:/RenderQMLPlugin/shaders/es2/default.vert")
            fragmentShaderCode: hasTexture?loadSource("qrc:/RenderQMLPlugin/shaders/es2/diffusemap.frag")
                                          :loadSource("qrc:/RenderQMLPlugin/shaders/es2/diffusemap_solidcolor.frag")
        }

        // This shader does not work properly yet on android
        ShaderProgram {
            id: es2ShadowShader
            vertexShaderCode: loadSource("qrc:/RenderQMLPlugin/shaders/es2/shadowmap.vert")
            fragmentShaderCode: loadSource("qrc:/RenderQMLPlugin/shaders/es2/shadowmap.frag")
        }

        PolygonOffset {
            id: polygonOffset
            scaleFactor: 4
            depthSteps: 4
        }

        DepthTest {
            id: depthTest
            depthFunction: DepthTest.Less
        }

        NoDepthMask {
            id: noDepthMask
            enabled: alpha < 1
        }

        CullFace {
            id: backFaceCulling
            mode: CullFace.Back
        }

        CullFace {
            id: shadowBackFaceCulling
            mode: CullFace.Back
        }

        BlendEquationArguments {
            id: blendEquationArguments
            sourceRgb: BlendEquationArguments.SourceAlpha
            destinationRgb: BlendEquationArguments.OneMinusSourceAlpha
            sourceAlpha: BlendEquationArguments.Zero
            destinationAlpha: BlendEquationArguments.One
            enabled: alpha < 1
        }

        BlendEquation {
            id: blendEquation
            blendFunction: BlendEquation.Add
            enabled: alpha < 1
        }

        techniques: [
            Technique {
                id: desktopTechnique

                filterKeys: [
                    techniqueFilterKey
                ]

                graphicsApiFilter {
                    api: GraphicsApiFilter.OpenGL
                    profile: GraphicsApiFilter.CoreProfile
                    majorVersion: 3
                    minorVersion: 1
                }

                // Per Technique Shadow bias
                parameters: [
                    Parameter { name: "shadowBias"; value: 0.002 }
                ]

                renderPasses: [
                    RenderPass {
                        id: gl3ShadowPass

                        enabled: alpha > 0 // workaround for FilterKey enabled not working

                        shaderProgram: gl3ShadowShader

                        filterKeys: [
                            shadowRenderPassFilterKey
                        ]

                        renderStates: [
                            shadowBackFaceCulling,
                            polygonOffset,
                            depthTest
                        ]
                    },
                    RenderPass {
                        id: gl3RenderPass

                        shaderProgram: gl3Shader

                        filterKeys: [
                            transparentRenderPassFilterKey
                        ]

                        renderStates: [
                            backFaceCulling,
                            noDepthMask,
                            blendEquation,
                            blendEquationArguments
                        ]
                    }
                ]
            },
            Technique {
                id: androidTechnique

                filterKeys: [
                    techniqueFilterKey
                ]

                graphicsApiFilter {
                    api: GraphicsApiFilter.OpenGLES
                    profile: GraphicsApiFilter.NoProfile
                    majorVersion: 2
                    minorVersion: 0
                }

                // Per Technique Shadow bias
                parameters: [
                    Parameter { name: "shadowBias"; value: 0.002 }
                ]

                renderPasses: [
                    RenderPass {
                        id: es2RenderPass

                        shaderProgram: es2Shader

                        filterKeys: [
                            transparentRenderPassFilterKey
                        ]

                        renderStates: [
                            backFaceCulling,
                            noDepthMask,
                            blendEquation,
                            blendEquationArguments
                        ]
                    }
                    ,
                    RenderPass {
                        id: es2ShadowPass

                        enabled: alpha > 0 // workaround for FilterKey enabled not working

                        shaderProgram: es2ShadowShader

                        filterKeys: [
                            shadowRenderPassFilterKey
                        ]

                        renderStates: [
                            shadowBackFaceCulling,
                            polygonOffset,
                            depthTest
                        ]
                    }
                ]
            }
        ]
    }
}
