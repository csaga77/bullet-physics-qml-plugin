/*!
RenderQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#pragma once

#include <QObject>
#include <QQuickWindow>

class QTimer;
class QElapsedTimer;

class FPSMonitor : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qreal instantaneousFrequency READ instantaneousFrequency NOTIFY instantaneousFrequencyChanged FINAL)
    Q_PROPERTY(qreal averageFrequency READ averageFrequency NOTIFY averageFrequencyChanged FINAL)
    Q_PROPERTY(qint32 samplingInterval READ samplingInterval WRITE setSamplingInterval NOTIFY samplingIntervalChanged FINAL)
    Q_PROPERTY(QQuickWindow *currentWindow READ currentWindow WRITE setCurrentWindow NOTIFY currentWindowChanged FINAL)
    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged FINAL)
public:
    explicit FPSMonitor(QObject *parent = nullptr);

    qreal instantaneousFrequency() const;
    qreal averageFrequency() const;
    qint32 samplingInterval() const;
    QQuickWindow * currentWindow() const;
    bool enabled() const;

signals:

    void instantaneousFrequencyChanged(const qreal &instantaneousFrequency);
    void averageFrequencyChanged(const qreal &averageFrequency);
    void samplingIntervalChanged(const qint32 &samplingInterval);
    void currentWindowChanged(QQuickWindow * currentWindow);
    void enabledChanged(const bool &enabled);

public slots:
    void update();
    void setSamplingInterval(const qint32 &samplingInterval);
    void setCurrentWindow(QQuickWindow * currentWindow);
    void setEnabled(const bool &enabled);

private:

    void calculateAverageFrequency();
    void setInstantaneousFrequency(const qreal &freq);
    void setAverageFrequency(const qreal &freq);
    void stalled();

    qreal m_instantaneousFrequency;
    qreal m_averageFrequency;

    qint32 m_count;
    QTimer *m_averageTimer;
    QTimer *m_stalledTimer;
    QSharedPointer<QElapsedTimer> m_instantaneousElapsed;
    QSharedPointer<QElapsedTimer> m_averageElapsed;
    qint32 m_samplingInterval;
    QQuickWindow * m_currentWindow;
    bool m_enabled;
};
