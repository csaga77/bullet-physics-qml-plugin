/*!
RenderQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <QComponent>
#include <QEntity>
#include <QQmlApplicationEngine>
#include <QSceneLoader>
#include <QGuiApplication>
#include <QQmlContext>
#include <QSurfaceFormat>
#include <QWindow>
#include "FpsMonitor.h"
#include "RenderQMLPlugin.h"

#include <qqml.h>

RenderQMLPlugin::RenderQMLPlugin()
{

}

void RenderQMLPlugin::registerTypes(const char *uri)
{
    // @uri QRenderQMLPlugin
    Q_ASSERT(uri == QLatin1String(RENDERPLUGIN_URI));

    qmlRegisterType<FPSMonitor>(RENDERPLUGIN_URI, RENDERPLUGIN_MAJOR, RENDERPLUGIN_MINOR, "FPSMonitor");

    qmlRegisterType(QUrl("qrc:/RenderQMLPlugin/Ball.qml"), RENDERPLUGIN_URI, RENDERPLUGIN_MAJOR, RENDERPLUGIN_MINOR, "Ball");
    qmlRegisterType(QUrl("qrc:/RenderQMLPlugin/Box.qml"), RENDERPLUGIN_URI, RENDERPLUGIN_MAJOR, RENDERPLUGIN_MINOR, "Box");
    qmlRegisterType(QUrl("qrc:/RenderQMLPlugin/Cylinder.qml"), RENDERPLUGIN_URI, RENDERPLUGIN_MAJOR, RENDERPLUGIN_MINOR, "Cylinder");
    qmlRegisterType(QUrl("qrc:/RenderQMLPlugin/DiffusemapMaterial.qml"), RENDERPLUGIN_URI, RENDERPLUGIN_MAJOR, RENDERPLUGIN_MINOR, "DiffusemapMaterial");
    qmlRegisterType(QUrl("qrc:/RenderQMLPlugin/MetalRoughMaterial.qml"), RENDERPLUGIN_URI, RENDERPLUGIN_MAJOR, RENDERPLUGIN_MINOR, "MetalRoughMaterial");
    qmlRegisterType(QUrl("qrc:/RenderQMLPlugin/FpsText.qml"), RENDERPLUGIN_URI, RENDERPLUGIN_MAJOR, RENDERPLUGIN_MINOR, "FpsText");
    qmlRegisterType(QUrl("qrc:/RenderQMLPlugin/ShadowFrameGraph.qml"), RENDERPLUGIN_URI, RENDERPLUGIN_MAJOR, RENDERPLUGIN_MINOR, "ShadowFrameGraph");
    qmlRegisterType(QUrl("qrc:/RenderQMLPlugin/ShadowFrameGraphViewport.qml"), RENDERPLUGIN_URI, RENDERPLUGIN_MAJOR, RENDERPLUGIN_MINOR, "ShadowFrameGraphViewport");

    qmlProtectModule(uri, 1);
}

void RenderQMLPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    // InitalizeEngine happens after the call to registerTypes
    QQmlExtensionPlugin::initializeEngine(engine, uri);
}


