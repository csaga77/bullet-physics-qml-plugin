#RenderQMLPlugin

#Copyright (c) 2018

#Bin Chen

#This software is provided 'as-is', without any express or implied warranty. In
#no event will the authors be held liable for any damages arising from the use
#of this software. Permission is granted to anyone to use this software for any
#purpose, including commercial applications, and to alter it and redistribute it
#freely, subject to the following restrictions:

#1. The origin of this software must not be misrepresented; you must not claim
#that you wrote the original software. If you use this software in a product, an
#acknowledgment in the product documentation would be appreciated but is not
#required.

#2. Altered source versions must be plainly marked as such, and must not be
#misrepresented as being the original software.

#3. This notice may not be removed or altered from any source distribution.

TEMPLATE = lib
TARGET = RenderQMLPlugin

QT += core qml quick gui
QT +=   3dcore \
        3drender \
        3dinput \
        3dquick \
        3dquickscene2d \
        3dextras \
        3dquickextras \
        3dlogic \
        3dquickinput \
        3dquickrender

QT += qml-private # This is needed when building a Qml plugin AND using QtQuickCompiler

CONFIG += qt plugin staticlib

DEFINES += QT_DEPRECATED_WARNINGS

DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x050900

DEFINES += QT_NO_NARROWING_CONVERSIONS_IN_CONNECT


DEFINES += \
        QT_USE_QSTRINGBUILDER \
        QT_USE_FAST_CONCATENATION \
        QT_USE_FAST_OPERATOR_PLUS \
        QT_NO_URL_CAST_FROM_STRING \
        QT_NO_CAST_TO_ASCII

SOURCES += \
    RenderQMLPlugin.cpp \
    FpsMonitor.cpp

HEADERS += \
    RenderQMLPlugin.h \
    FpsMonitor.h

RESOURCES += \
    RenderQMLPlugin.qrc

DISTFILES += \
    qmldir \
    pluginmetadata.json


# deploy plugin
!staticlib {
    #For dynamic link plugin
    pluginURI = RenderQMLPlugin
    pluginVersion = 1.0

    # custom var DESTPATH to store the install path
    DESTPATH = $$[QT_INSTALL_QML]/$$replace(pluginURI, \\., /)

    # set target path - will automatically include the built binary
    target.path = $$DESTPATH
    INSTALLS += target

    # add the qmldir and other qml supporting files for install
    qmlfiles.files = $$PWD/qmldir $$PWD/*.js $$OUT_PWD/plugins.qmltypes #$$PWD/*.qml
    qmlfiles.path = $$DESTPATH
    INSTALLS += qmlfiles

    # add the plugins.qmltypes files for install
    # note that plugins.qmltypes file has to be generated from the install path.
    win32 {
        qmltypes.files = $$PWD/plugins.qmltypes
        qmltypes.path = $$DESTPATH
        qmltypes.extra = qmlplugindump -nonrelocatable $$pluginURI $$pluginVersion > $$PWD/plugins.qmltypes
        qmltypes.depends = install_target
        INSTALLS += qmltypes
    }

    # don't call qmlplugindump when compiling for android (as it looks for windows component plugin libraries)
    android {
        qmltypes.files = $$PWD/plugins.qmltypes
        qmltypes.path = $$DESTPATH
        qmltypes.depends = install_target
        INSTALLS += qmltypes
    }

}
