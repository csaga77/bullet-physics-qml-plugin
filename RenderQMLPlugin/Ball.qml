import Qt3D.Core 2.0
import Qt3D.Extras 2.0
import Qt3D.Render 2.0

Entity {
    id: root

    property alias position: transform.translation
    property alias rotation: transform.rotation
    property alias radius: ballMesh.radius
    property alias matrix: transform.matrix
    property Material material

    SphereMesh {
        id: ballMesh
        rings: 32
        slices: 32
        radius: 10
    }

    Transform {
        id: transform
    }

    components: [
        ballMesh,
        transform,
        material
    ]
}
