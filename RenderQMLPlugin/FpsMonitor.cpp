/*!
RenderQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include "FpsMonitor.h"

#include <QTimer>
#include <QElapsedTimer>

FPSMonitor::FPSMonitor(QObject *parent) :
    QObject(parent),
    m_averageTimer(new QTimer(this)),
    m_stalledTimer(new QTimer(this)),
    // QElapsedTimer doens't inherit QObject so we manage the memory using a shared pointer.
    m_instantaneousElapsed(QSharedPointer<QElapsedTimer>(new QElapsedTimer)),
    m_averageElapsed(QSharedPointer<QElapsedTimer>(new QElapsedTimer))
{
    m_instantaneousFrequency = 0;
    m_averageFrequency = 0;

    m_count = 0;
    m_samplingInterval = 2000;

    m_instantaneousElapsed->start();
    m_averageElapsed->start();

    connect(m_averageTimer, &QTimer::timeout, this, &FPSMonitor::calculateAverageFrequency);
    m_averageTimer->start(m_samplingInterval);

    connect(m_stalledTimer, &QTimer::timeout, this, &FPSMonitor::stalled);
    m_stalledTimer->setSingleShot(true);

    m_currentWindow = nullptr;

    m_enabled = false;
}

qreal FPSMonitor::instantaneousFrequency() const
{
    return m_instantaneousFrequency;
}

qreal FPSMonitor::averageFrequency() const
{
    return m_averageFrequency;
}

qint32 FPSMonitor::samplingInterval() const
{
    return m_samplingInterval;
}

QQuickWindow *FPSMonitor::currentWindow() const
{
    return m_currentWindow;
}

bool FPSMonitor::enabled() const
{
    return m_enabled;
}

void FPSMonitor::update()
{
    if(!m_enabled)
        return;

    m_count++; // used by calculateAverageFrequency()

    const qint64 ms = m_instantaneousElapsed->restart();
    qreal instantaneousFrequency = ms ? qreal(1000) / ms : 0;

    // we only emit integer properties
    setInstantaneousFrequency(instantaneousFrequency);
}

void FPSMonitor::setSamplingInterval(const qint32 &samplingInterval)
{
    if (m_samplingInterval == samplingInterval)
        return;

    if(samplingInterval > 0) {
        m_averageTimer->setInterval(samplingInterval);
        m_averageTimer->start();
    }
    else {
        m_averageTimer->stop();
    }

    m_samplingInterval = samplingInterval;
    emit samplingIntervalChanged(samplingInterval);
}

void FPSMonitor::setCurrentWindow(QQuickWindow *window)
{
    if (m_currentWindow == window)
        return;

    if(m_currentWindow)
        disconnect(m_currentWindow, &QQuickWindow::afterRendering, this, &FPSMonitor::update);

    m_currentWindow = window;

    if(m_currentWindow)
        connect(m_currentWindow, &QQuickWindow::afterRendering, this, &FPSMonitor::update, Qt::QueuedConnection);

    emit currentWindowChanged(window);
}

void FPSMonitor::setEnabled(const bool &enabled)
{
    if (m_enabled == enabled)
        return;

    m_enabled = enabled;
    emit enabledChanged(enabled);
}

void FPSMonitor::calculateAverageFrequency()
{
    const qint64 ms = m_averageElapsed->restart();
    qreal averageFrequency = qreal(m_count * 1000) / ms;
    m_stalledTimer->start(static_cast<int>(3 * ms));

    // we only emit integer properties
    setAverageFrequency(averageFrequency);

    m_count = 0;
}

void FPSMonitor::setInstantaneousFrequency(const qreal &freq)
{
    if(qFuzzyCompare(m_instantaneousFrequency, freq))
        return;

    m_instantaneousFrequency = freq;

    emit instantaneousFrequencyChanged(m_instantaneousFrequency);
}

void FPSMonitor::setAverageFrequency(const qreal &freq)
{
    if(qFuzzyCompare(m_averageFrequency, freq))
        return;

    m_averageFrequency = freq;

    emit averageFrequencyChanged(m_averageFrequency);
}

void FPSMonitor::stalled()
{
    if (m_instantaneousFrequency > 0) {
        m_instantaneousFrequency = 0;
        emit instantaneousFrequencyChanged(m_instantaneousFrequency);
    }
}

#include "moc_FpsMonitor.cpp"
