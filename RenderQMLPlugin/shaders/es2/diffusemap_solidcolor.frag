precision highp float;
//es2/diffusemap_solidcolor.frag

uniform vec4 ambientColor;            // Ambient reflectivity
uniform vec4 specularColor;            // Specular reflectivity
uniform float shininess;    // Specular shininess factor

uniform sampler2D diffuseTexture;
uniform vec4 diffuseColor;

uniform float alphaValue;

varying vec3 worldPosition;
varying vec3 worldNormal;
varying vec3 worldView;
varying vec2 texCoord;
varying vec4 positionInLightSpace;

#pragma include phong.inc.frag
#pragma include shadowmap.inc.frag

void main()
{
    vec4 diffuse = diffuseColor;

    if (shadowMapFactor(worldPosition, worldNormal, positionInLightSpace) > 0.0) {
        //Not In the shadow
        gl_FragColor = phongFunction(ambientColor, diffuse, specularColor, shininess, worldPosition, worldView, worldNormal, alphaValue);
    } else {
        gl_FragColor = ambientColor * diffuse;
    }

    gl_FragColor = colorCorrection(gl_FragColor);
    gl_FragColor.a = gl_FragColor.a* alphaValue;
}
