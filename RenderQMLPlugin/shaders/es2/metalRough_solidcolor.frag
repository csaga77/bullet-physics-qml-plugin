precision highp float;
//es2/diffusemap_solidcolor.frag

uniform vec4 ambientColor;            // Ambient reflectivity
uniform float metalness;
uniform float roughness;
uniform float ambientOcclusion;
uniform sampler2D diffuseTexture;
uniform vec4 diffuseColor;

uniform float alphaValue;

varying vec3 worldPosition;
varying vec3 worldNormal;
varying vec3 worldView;
varying vec2 texCoord;
varying vec4 positionInLightSpace;

//#pragma include phong.inc.frag
#pragma include metalrough.inc.frag
#pragma include shadowmap.inc.frag

void main()
{
    vec4 diffuse = diffuseColor;
    float tmpAmbientOcclusion = 1.0;
    if (shadowMapFactor(worldPosition, worldNormal, positionInLightSpace) > 0.0) {
        //Not in shadow
    } else {
        tmpAmbientOcclusion = ambientColor.r;
    }

    diffuse = metalRoughFunction(diffuse,
                                 metalness,
                                 roughness,
                                 tmpAmbientOcclusion,
                                 worldPosition,
                                 worldView,
                                 worldNormal
                                 );
    diffuse.a = diffuse.a * alphaValue;
    gl_FragColor = diffuse;
}
