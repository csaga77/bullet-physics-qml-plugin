//es2/shadowmpa.vert
uniform mat4 modelViewProjection;
attribute vec3 vertexPosition;

void main()
{
    gl_Position = modelViewProjection * vec4(vertexPosition, 1.0);
}
