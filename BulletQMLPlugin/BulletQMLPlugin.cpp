/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <QComponent>
#include <QEntity>
#include <QQmlApplicationEngine>
#include <QSceneLoader>
#include <QGuiApplication>
#include <QQmlContext>
#include <QSurfaceFormat>
#include <QWindow>

#include <QBullet/AutoHeightmapImpactModifier.h>
#include <QBullet/BoxShape.h>
#include <QBullet/BulletObject.h>
#include <QBullet/CollisionObject.h>
#include <QBullet/CollisionShape.h>
#include <QBullet/CompoundShape.h>
#include <QBullet/ConeShape.h>
#include <QBullet/ConeTwistConstraint.h>
#include <QBullet/Constraint.h>
#include <QBullet/ContactCallback.h>
#include <QBullet/ConvexShape.h>
#include <QBullet/CapsuleShape.h>
#include <QBullet/CylinderShape.h>
#include <QBullet/DiscreteDynamicsWorld.h>
#include <QBullet/DummyTriangleMesh.h>
#include <QBullet/FixedConstraint.h>
#include <QBullet/GearConstraint.h>
#include <QBullet/Generic6DofConstraint.h>
#include <QBullet/Generic6DofSpring2Constraint.h>
#include <QBullet/Generic6DofSpringConstraint.h>
#include <QBullet/HeightfieldTerrainShape.h>
#include <QBullet/Heightmap.h>
#include <QBullet/HeightmapModifier.h>
#include <QBullet/Hinge2Constraint.h>
#include <QBullet/HingeAccumulatedAngleConstraint.h>
#include <QBullet/HingeConstraint.h>
#include <QBullet/Point2PointConstraint.h>
#include <QBullet/RayTest.h>
#include <QBullet/RigidBody.h>
#include <QBullet/SliderConstraint.h>
#include <QBullet/SphereShape.h>
#include <QBullet/StaticPlaneShape.h>
#include <QBullet/TriangleMesh.h>
#include <QBullet/TriangleMeshShape.h>
#include <QBullet/UniformScalingShape.h>
#include <QBullet/UniversalConstraint.h>

#include <QBulletRender/TriangleMeshRenderer.h>
#include "BulletQMLPlugin.h"

#include <qqml.h>


#define QB_REG(x) qmlRegisterType<QBullet::x>(BULLETPLUGIN_URI, BULLETPLUGIN_MAJOR, BULLETPLUGIN_MINOR, #x)
#define QB_REG_UNCREATABLE(x, msg) qmlRegisterUncreatableType<QBullet::x>(BULLETPLUGIN_URI, BULLETPLUGIN_MAJOR, BULLETPLUGIN_MINOR, #x, #msg)

BulletQMLPlugin::BulletQMLPlugin()
{

}

void BulletQMLPlugin::registerTypes(const char *uri)
{
    // @uri QBullet
    Q_ASSERT(uri == QLatin1String(BULLETPLUGIN_URI));

    QB_REG_UNCREATABLE(BulletObject, "Abstract base class BulletObject can't be created");
    QB_REG_UNCREATABLE(CollisionObject, "Abstract base class CollisionObject can't be created");
    QB_REG_UNCREATABLE(CollisionShape, "Abstract base class CollisionShape can't be created");
    QB_REG_UNCREATABLE(Compound, "Attaching type Compound is uncreatable. It provides attached properties to a CompoundShape's children shapes");
    QB_REG_UNCREATABLE(Constraint, "Base class Constraint can't be created");
    QB_REG_UNCREATABLE(ContactCallback, "Abstract base class ContactCallback can't be created");
    QB_REG_UNCREATABLE(ConvexShape, "Abstract base class ConvexShape can't be created");
    QB_REG_UNCREATABLE(TriangleMesh, "TriangleMesh is abstract class and can't be created.");

    //qmlRegisterType<QBullet::BoxShape>(QBULLET_URI, QBULLET_MAJOR, QBULLET_MINOR, "BoxShape");
    QB_REG(AutoHeightmapImpactModifier);
    QB_REG(BoxShape);
    QB_REG(CapsuleShape);
    QB_REG(CompoundShape);
    QB_REG(ConeShape);
    QB_REG(ConeTwistConstraint);
    QB_REG(CylinderShape);
    QB_REG(DiscreteDynamicsWorld);
    QB_REG(DummyTriangleMesh);
    QB_REG(FixedConstraint);
    QB_REG(GearConstraint);
    QB_REG(Generic6DofConstraint);
    QB_REG(Generic6DofSpring2Constraint);
    QB_REG(Generic6DofSpringConstraint);
    QB_REG(HeightfieldTerrainShape);
    QB_REG(Heightmap);
    QB_REG(HeightmapModifier);
    QB_REG(Hinge2Constraint);
    QB_REG(HingeAccumulatedAngleConstraint);
    QB_REG(HingeConstraint);
    QB_REG(Point2PointConstraint);
    QB_REG(RayTest);
    QB_REG(RigidBody);
    QB_REG(SliderConstraint);
    QB_REG(SphereShape);
    QB_REG(StaticPlaneShape);
    QB_REG(TriangleMeshRenderer);
    QB_REG(TriangleMeshShape);
    QB_REG(UniformScalingShape);
    QB_REG(UniversalConstraint);

    qmlRegisterType(QUrl("qrc:/BulletQMLPlugin/Picker.qml"),
                    BULLETPLUGIN_URI,
                    BULLETPLUGIN_MAJOR, BULLETPLUGIN_MINOR,
                    "Picker");

    qmlProtectModule(uri, 1);
}

void BulletQMLPlugin::initializeEngine(QQmlEngine *engine, const char *uri)
{
    // InitalizeEngine happens after the call to registerTypes
    QQmlExtensionPlugin::initializeEngine(engine, uri);
}


