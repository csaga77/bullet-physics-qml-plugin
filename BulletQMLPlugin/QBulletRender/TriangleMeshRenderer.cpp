/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <Qt3DRender>
#include <QtDebug>
#include <QAttribute>
#include <QGeometry>
#include <QGeometryFactory>
#include <QBullet/btIndexedMeshHelper.h>
#include "TriangleMeshGeometry.h"
#include "TriangleMeshRenderer.h"

namespace QBullet {

using namespace Qt3DRender;

class TriangleMeshFunctor : public QGeometryFactory
{
public:
    QT3D_FUNCTOR(TriangleMeshFunctor)

    TriangleMeshFunctor(TriangleMesh *mesh)
        : m_mesh(mesh)
    {}

    ~TriangleMeshFunctor() {}

    QGeometry *operator ()() Q_DECL_OVERRIDE
    {
        return new TriangleMeshGeometry(m_mesh);
    }

    bool operator ==(const QGeometryFactory &other) const Q_DECL_OVERRIDE
    {
        const TriangleMeshFunctor *meshFunc = functor_cast<TriangleMeshFunctor>(&other);

        return meshFunc?meshFunc->m_mesh==m_mesh:false;
    }


private:
    TriangleMesh *m_mesh;

};

TriangleMeshRenderer::TriangleMeshRenderer(QNode *parent)
    : Qt3DRender::QGeometryRenderer(parent)
    , m_mesh(0)
{
    setPrimitiveType(Qt3DRender::QGeometryRenderer::Triangles);
}

TriangleMeshRenderer::~TriangleMeshRenderer()
{

}


TriangleMesh *TriangleMeshRenderer::mesh() const
{
    return m_mesh;
}

void TriangleMeshRenderer::setMesh(TriangleMesh *mesh)
{
    if (m_mesh == mesh)
        return;
    if(m_mesh) {
        m_mesh->disconnect(this);
    }
    m_mesh = mesh;
    if(m_mesh) {
        connect(m_mesh, &TriangleMesh::meshReset, this, &TriangleMeshRenderer::resetMesh);
        resetMesh();
    }
    emit meshChanged(m_mesh);
}

void TriangleMeshRenderer::resetMesh()
{
    QGeometryRenderer::setGeometryFactory(QGeometryFactoryPtr(0));
    if(m_mesh) {
        QGeometryRenderer::setGeometryFactory(QGeometryFactoryPtr(new TriangleMeshFunctor(m_mesh)));
    }
}


}

#include "moc_TriangleMeshRenderer.cpp"
