/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "CompoundShape.h"
#include "QBullet.h"

namespace QBullet {

CompoundShape::CompoundShape(QObject *parent)
    : CollisionShape(parent)
{

}

QSharedPointer<btCompoundShape> CompoundShape::compoundShape() const
{
    return shape().dynamicCast<btCompoundShape>();
}

void CompoundShape::onSubShapeReset()
{
    QBullet::CollisionShape *shape = qobject_cast<QBullet::CollisionShape *>(sender());
    QSharedPointer<btCollisionShape> btShape = _btShapes.take(shape);
    if(btShape) {
        if(compoundShape())
            compoundShape()->removeChildShape(btShape.data());
    }
    if(shape->shape()) {
        CompoundShapeAttachedType *attached = qobject_cast<CompoundShapeAttachedType *>(qmlAttachedPropertiesObject<Compound>(shape));
        Q_ASSERT(attached);
        _btShapes.insert(shape, shape->shape());
        if(compoundShape())
            compoundShape()->addChildShape(q2b(attached->matrix()), shape->shape().data());
    }
    emit shapeReset();
}

void CompoundShape::resetSubShapes()
{
    foreach(CollisionShape *shape, _shapes) {
        if(shape->shape().isNull())
            continue;
        CompoundShapeAttachedType *attached = qobject_cast<CompoundShapeAttachedType *>(qmlAttachedPropertiesObject<Compound>(shape));
        if(compoundShape()) {
            compoundShape()->removeChildShape(shape->shape().data());
            compoundShape()->addChildShape(q2b(attached->matrix()), shape->shape().data());
        }
    }
    emit shapeReset();
}

void CompoundShape::addShape(QBullet::CollisionShape *shape)
{
    _shapes<<shape;
    connect(shape, &CollisionShape::shapeReset, this, &CompoundShape::onSubShapeReset);
    connect(shape, &CollisionShape::propertyChanged, this, &CompoundShape::propertyChanged);
    CompoundShapeAttachedType *attached = qobject_cast<CompoundShapeAttachedType *>(qmlAttachedPropertiesObject<Compound>(shape));
    connect(attached, &CompoundShapeAttachedType::matrixChanged, this, &CompoundShape::resetSubShapes);
    Q_ASSERT(attached);
    if(shape->shape()) {
        _btShapes.insert(shape, shape->shape());
        if(compoundShape()) {
            compoundShape()->addChildShape(q2b(attached->matrix()), shape->shape().data());
        }
        emit shapeReset();
    }
}

void CompoundShape::clear()
{
    CollisionShape::clear();
    QList<QSharedPointer<btCollisionShape>> shapes = _btShapes.values();
    if(compoundShape()) {
        foreach(QSharedPointer<btCollisionShape> btShape, shapes) {
            compoundShape()->removeChildShape(btShape.data());
        }
    }
    _shapes.clear();
    _btShapes.clear();
}

void CompoundShape::removeShape(QBullet::CollisionShape *shape)
{
    _shapes.removeAll(shape);
    QSharedPointer<btCollisionShape> btShape = _btShapes.take(shape);
    if(btShape) {
        if(compoundShape()) {
            compoundShape()->removeChildShape(btShape.data());
        }
        emit shapeReset();
    }
}

void CompoundShape::appendShape(QQmlListProperty<CollisionShape> *list, CollisionShape *shape)
{
    CompoundShape *cs = qobject_cast<CompoundShape *>(list->object);
    Q_ASSERT(cs);
    cs->addShape(shape);
}

int CompoundShape::countShapes(QQmlListProperty<CollisionShape> *list)
{
    CompoundShape *cs = qobject_cast<CompoundShape *>(list->object);
    Q_ASSERT(cs);
    return cs->_shapes.size();
}

CollisionShape *CompoundShape::atShape(QQmlListProperty<CollisionShape> *list, int index)
{
    CompoundShape *cs = qobject_cast<CompoundShape *>(list->object);
    Q_ASSERT(cs);
    return cs->_shapes.at(index);
}

void CompoundShape::clearShapes(QQmlListProperty<CollisionShape> *list)
{
    CompoundShape *cs = qobject_cast<CompoundShape *>(list->object);
    Q_ASSERT(cs);
    cs->clear();
}

QSharedPointer<btCollisionShape> CompoundShape::create() const
{
    QSharedPointer<btCompoundShape> newShape = QSharedPointer<btCompoundShape>(new btCompoundShape);
    foreach(CollisionShape *shape, _shapes) {
        if(shape->shape().isNull())
            continue;
        CompoundShapeAttachedType *attached = qobject_cast<CompoundShapeAttachedType *>(qmlAttachedPropertiesObject<Compound>(shape));
        newShape->addChildShape(q2b(attached->matrix()), shape->shape().data());
    }
    return newShape;
}


CompoundShapeAttachedType::CompoundShapeAttachedType(QObject *parent)
    : QObject(parent)
{

}

QVector3D CompoundShapeAttachedType::position() const
{
    return m_position;
}

QQuaternion CompoundShapeAttachedType::rotation() const
{
    return m_rotation;
}

QMatrix4x4 CompoundShapeAttachedType::matrix() const
{
    return m_matrix;
}

qreal CompoundShapeAttachedType::yaw() const
{
    return rotation().toEulerAngles().y();
}

qreal CompoundShapeAttachedType::pitch() const
{
    return rotation().toEulerAngles().x();
}

qreal CompoundShapeAttachedType::roll() const
{
    return rotation().toEulerAngles().z();
}
void CompoundShapeAttachedType::setPosition(QVector3D position)
{
    if (m_position == position)
        return;

    m_position = position;
    updateMatrix();
    emit positionChanged(m_position);

}

void CompoundShapeAttachedType::setRotation(QQuaternion rotation)
{
    if (m_rotation == rotation)
        return;

    m_rotation = rotation;
    updateMatrix();
    emit rotationChanged(m_rotation);

}

void CompoundShapeAttachedType::setMatrix(QMatrix4x4 matrix)
{
    if (m_matrix == matrix)
        return;

    m_matrix = matrix;
    emit matrixChanged(m_matrix);
}

void CompoundShapeAttachedType::setYaw(qreal yaw)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->yaw(), yaw))
        return;

    setRotation(QQuaternion::fromEulerAngles(pitch(), yaw, roll()));

}

void CompoundShapeAttachedType::setPitch(qreal pitch)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->pitch(), pitch))
        return;

    //qDebug()<<"RigidBody()"<<pitch;
    setRotation(QQuaternion::fromEulerAngles(pitch, yaw(), roll()));

}

void CompoundShapeAttachedType::setRoll(qreal roll)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->roll(), roll))
        return;

    setRotation(QQuaternion::fromEulerAngles(pitch(), yaw(), roll));

}


void CompoundShapeAttachedType::updateMatrix()
{
    QMatrix4x4 mat;
    mat.translate(m_position);
    mat.rotate(m_rotation);
    setMatrix(mat);
}

}

#include "moc_CompoundShape.cpp"
