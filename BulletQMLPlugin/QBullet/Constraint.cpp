/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "Constraint.h"
#include "DiscreteDynamicsWorld.h"
#include "RigidBody.h"

namespace QBullet
{

Constraint::Constraint(QObject *parent)
    : BulletObject(parent)
    , m_constraint(0)
    , m_rigidBodyA(0)
    , m_rigidBodyB(0)
    , m_disableCollisionsBetweenLinkedBodies(true)
{

}

bool Constraint::hasConstraint() const
{
    return !m_constraint.isNull();
}

QSharedPointer<btTypedConstraint> Constraint::constraint() const
{
    return m_constraint;
}

RigidBody *Constraint::rigidBodyA() const
{
    return m_rigidBodyA;
}

RigidBody *Constraint::rigidBodyB() const
{
    return m_rigidBodyB;
}

DiscreteDynamicsWorld *Constraint::world() const
{
    return m_world;
}

void Constraint::setRigidBodyA(RigidBody *rigidBodyA)
{
    if (m_rigidBodyA == rigidBodyA)
        return;
    if (m_rigidBodyA) {
        m_rigidBodyA->disconnect(this);
    }
    clear();
    m_rA.clear();
    m_rigidBodyA = rigidBodyA;
    if(m_rigidBodyA) {
        connect(m_rigidBodyA, &RigidBody::destroyed, this, &Constraint::bodyDestroyed);
        connect(m_rigidBodyA, &RigidBody::objectReset, this, &Constraint::resetConstraint);
    }
    emit rigidBodyAChanged(m_rigidBodyA);
    resetConstraint();
}

void Constraint::setRigidBodyB(RigidBody *rigidBodyB)
{
    if (m_rigidBodyB == rigidBodyB)
        return;
    if (m_rigidBodyB) {
        m_rigidBodyB->disconnect(this);
    }
    clear();
    m_rB.clear();
    m_rigidBodyB = rigidBodyB;
    if(m_rigidBodyB) {
        connect(m_rigidBodyB, &RigidBody::destroyed, this, &Constraint::bodyDestroyed);
        connect(m_rigidBodyB, &RigidBody::objectReset, this, &Constraint::resetConstraint);
    }
    emit rigidBodyBChanged(m_rigidBodyB);
    resetConstraint();
}

void Constraint::setWorld(DiscreteDynamicsWorld *world)
{
    if (m_world == world)
        return;
    if(m_worldData&&m_constraint) {
        m_worldData->dynamicsWorld->removeConstraint(m_constraint.data());
    }
    m_worldData.clear();
    m_world = world;
    if(m_world) {
        m_worldData = m_world->worldData();
        if(m_worldData&&m_constraint) {
            m_worldData->dynamicsWorld->addConstraint(m_constraint.data(), m_disableCollisionsBetweenLinkedBodies);
        }
    }
    emit worldChanged(m_world);
}

QSharedPointer<btRigidBody> Constraint::rbA() const
{
    return m_rA;
}

QSharedPointer<btRigidBody> Constraint::rbB() const
{
    return m_rB;
}

void Constraint::bodyDestroyed()
{
    if(sender()==m_rigidBodyA) {
        m_rigidBodyA = 0;
    } else if(sender()==m_rigidBodyB) {
        m_rigidBodyB = 0;
    }
    resetConstraint();
}

void Constraint::clear()
{
    if(m_worldData&&m_constraint) {
        m_worldData->dynamicsWorld->removeConstraint(m_constraint.data());
    }
    m_constraint.clear();
    m_rA.clear();
    m_rB.clear();
}

void Constraint::onComponentComplete()
{
    resetConstraint();
}

void Constraint::resetConstraint()
{
    if(!isComplete())
        return;
    clear();
    if((m_rigidBodyA&&!m_rigidBodyA->isComplete())||
            (m_rigidBodyB&&!m_rigidBodyB->isComplete()))
        return;
    if(m_rigidBodyA)
        m_rA = m_rigidBodyA->body();
    if(m_rigidBodyB)
        m_rB = m_rigidBodyB->body();
    if(m_constraint.isNull()) {
        m_constraint = create();
        if(m_worldData&&m_constraint) {
            m_worldData->dynamicsWorld->addConstraint(m_constraint.data(), m_disableCollisionsBetweenLinkedBodies);
        }
    }
    postCreate();
    if(m_rigidBodyA) {
        m_rigidBodyA->clearForces();
    }
    if(m_rigidBodyB) {
        m_rigidBodyB->clearForces();
    }
}

void Constraint::postCreate()
{

}

}
