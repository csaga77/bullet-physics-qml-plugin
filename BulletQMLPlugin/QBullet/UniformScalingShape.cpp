/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "UniformScalingShape.h"

namespace QBullet {

UniformScalingShape::UniformScalingShape(QObject *parent)
    : ConvexShape(parent)
    , m_childShape(0)
    , m_uniformScale(1.0)
{

}

ConvexShape *UniformScalingShape::childShape() const
{
    return m_childShape;
}

qreal UniformScalingShape::uniformScale() const
{
    return m_uniformScale;
}

QSharedPointer<btUniformScalingShape> UniformScalingShape::uniformScalingShape() const
{
    return shape().dynamicCast<btUniformScalingShape>();
}

void UniformScalingShape::setChildShape(ConvexShape *childShape)
{
    if (m_childShape == childShape)
        return;
    if(m_childShape) {
        m_childShape->disconnect(this);
    }
    clear();
    m_childShape = childShape;
    if(m_childShape) {
        m_childBtShape = m_childShape->convexShape();
        connect(m_childShape, &ConvexShape::destroyed, this, &UniformScalingShape::clear);
        connect(m_childShape, &ConvexShape::shapeReset, this, &UniformScalingShape::childShapeReset);
        connect(m_childShape, &ConvexShape::propertyChanged, this, &UniformScalingShape::propertyChanged);
    }
    resetShape();
    emit childShapeChanged(m_childShape);
}

void UniformScalingShape::setUniformScale(qreal uniformScale)
{
    if(uniformScale<=0)
        return;
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_uniformScale, uniformScale))
        return;

    m_uniformScale = uniformScale;
    resetShape();
    emit uniformScaleChanged(m_uniformScale);
}

void UniformScalingShape::childShapeReset()
{
    //Temporarily remember the old child until resetShape() is called.
    QSharedPointer<btCollisionShape> oldChild = m_childBtShape;
    m_childBtShape.clear();
    if(m_childShape) {
        m_childBtShape = m_childShape->convexShape();
    }
    resetShape();
}

void UniformScalingShape::clear()
{
    ConvexShape::clear();
    m_childBtShape.clear();
    m_childShape = 0;
}

QSharedPointer<btCollisionShape> UniformScalingShape::create() const
{
    if(m_childBtShape.isNull())
        return QSharedPointer<btCollisionShape>(0);
    return QSharedPointer<btCollisionShape>(new btUniformScalingShape(m_childBtShape.data(), m_uniformScale));
}

}
