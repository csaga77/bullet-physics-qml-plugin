/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef UNIFORMSCALINGSHAPE_H
#define UNIFORMSCALINGSHAPE_H

#include "ConvexShape.h"

class btUniformScalingShape;
namespace QBullet {

/*!
The UniformScalingShape allows to re-use uniform scaled instances of
ConvexShape in a memory efficient way.

Instead of using UniformScalingShape, it is better to use the non-uniform
setLocalScaling method on convex shapes that implement it.
*/
class UniformScalingShape : public ConvexShape
{
    Q_OBJECT
    Q_PROPERTY(QBullet::ConvexShape* childShape READ childShape WRITE setChildShape NOTIFY childShapeChanged)
    Q_PROPERTY(qreal uniformScale READ uniformScale WRITE setUniformScale NOTIFY uniformScaleChanged)
public:
    explicit UniformScalingShape(QObject *parent = nullptr);

    QBullet::ConvexShape *childShape() const;

    qreal uniformScale() const;

    QSharedPointer<btUniformScalingShape> uniformScalingShape() const;

signals:
    void childShapeChanged(QBullet::ConvexShape* childShape);

    void uniformScaleChanged(qreal uniformScale);

public slots:

    void setChildShape(QBullet::ConvexShape* childShape);

    void setUniformScale(qreal uniformScale);
protected slots:
    void childShapeReset();
    void clear() override;
protected:
    QSharedPointer<btCollisionShape> create() const override;

private:
    QBullet::ConvexShape* m_childShape;
    qreal m_uniformScale;
    QSharedPointer<btConvexShape> m_childBtShape;
};

}

#endif // UNIFORMSCALINGSHAPE_H
