/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef BULLETOBJECT_H
#define BULLETOBJECT_H

#include <QObject>
#include <QQmlParserStatus>

namespace QBullet {

class BulletObject : public QObject, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
public:
    explicit BulletObject(QObject *parent = nullptr);

    virtual ~BulletObject();

    enum Axis
    {
        Axis_X=0,
        Axis_Y,
        Axis_Z,
    };

    Q_ENUM(Axis)

    enum RotateOrder
    {
        RO_XYZ=0,
        RO_XZY,
        RO_YXZ,
        RO_YZX,
        RO_ZXY,
        RO_ZYX
    };
    Q_ENUM(RotateOrder)


    void classBegin() override;

    void componentComplete() override;

    bool isComplete() const;

protected:
    virtual void onComponentComplete();

private:
    bool m_isComponentComplete;
};

}

#endif // BULLETOBJECT_H
