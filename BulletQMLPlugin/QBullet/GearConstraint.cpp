/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "GearConstraint.h"
#include "QBullet.h"

namespace QBullet
{

GearConstraint::GearConstraint(QObject *parent)
    : Constraint(parent)
    , m_axisA(0, 0, 1)
    , m_axisB(0, 0, 1)
    , m_ratio(1.0)
{

}

QVector3D GearConstraint::axisA() const
{
    return m_axisA;
}

QVector3D GearConstraint::axisB() const
{
    return m_axisB;
}

qreal GearConstraint::ratio() const
{
    return m_ratio;
}

void GearConstraint::setAxisA(QVector3D axisA)
{
    if (m_axisA == axisA)
        return;

    m_axisA = axisA;
    if(gear()) {
        btVector3 tmp = q2b(axisA);
        gear()->setAxisA(tmp);
    }
    emit axisAChanged(m_axisA);
}

void GearConstraint::setAxisB(QVector3D axisB)
{
    if (m_axisB == axisB)
        return;

    m_axisB = axisB;
    if(gear()) {
        btVector3 tmp = q2b(axisB);
        gear()->setAxisB(tmp);
    }
    emit axisBChanged(m_axisB);
}

void GearConstraint::setRatio(qreal ratio)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_ratio, ratio))
        return;

    m_ratio = ratio;
    if(gear()) {
        gear()->setRatio(ratio);
    }
    emit ratioChanged(m_ratio);
}

QSharedPointer<btTypedConstraint> GearConstraint::create() const
{
    if(rbA()&&rbB()) {
        return QSharedPointer<btTypedConstraint>(new btGearConstraint(*rbA().data(), *rbB().data(),
                                                                      q2b(m_axisA), q2b(m_axisB), m_ratio));

    }
    return QSharedPointer<btTypedConstraint>(0);
}

QSharedPointer<btGearConstraint> GearConstraint::gear() const
{
    return constraint().dynamicCast<btGearConstraint>();
}

}
