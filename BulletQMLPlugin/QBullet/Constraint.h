/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef CONSTRAINT_H
#define CONSTRAINT_H

#include <QSharedPointer>
#include "BulletObject.h"

class btRigidBody;
class btTypedConstraint;

namespace QBullet
{
class DiscreteDynamicsWorld;
class RigidBody;
class WorldData;

class Constraint : public BulletObject
{
    Q_OBJECT
    Q_PROPERTY(QBullet::DiscreteDynamicsWorld* world READ world WRITE setWorld NOTIFY worldChanged)
    Q_PROPERTY(QBullet::RigidBody* rigidBodyA READ rigidBodyA WRITE setRigidBodyA NOTIFY rigidBodyAChanged)
    Q_PROPERTY(QBullet::RigidBody* rigidBodyB READ rigidBodyB WRITE setRigidBodyB NOTIFY rigidBodyBChanged)
public:
    explicit Constraint(QObject *parent = nullptr);

    bool hasConstraint() const;

    QSharedPointer<btTypedConstraint> constraint() const;

    QBullet::RigidBody *rigidBodyA() const;

    QBullet::RigidBody *rigidBodyB() const;

    QBullet::DiscreteDynamicsWorld *world() const;

signals:
    void rigidBodyAChanged(QBullet::RigidBody* rigidBodyA);

    void rigidBodyBChanged(QBullet::RigidBody* rigidBodyB);

    void worldChanged(QBullet::DiscreteDynamicsWorld* world);

public slots:

    void setRigidBodyA(QBullet::RigidBody* rigidBodyA);

    void setRigidBodyB(QBullet::RigidBody* rigidBodyB);

    void setWorld(QBullet::DiscreteDynamicsWorld* world);

    void clear();

protected slots:
    void resetConstraint();

protected:
    void onComponentComplete() override;

    virtual QSharedPointer<btTypedConstraint> create() const = 0;

    virtual void postCreate();

    QSharedPointer<btRigidBody> rbA() const;

    QSharedPointer<btRigidBody> rbB() const;

private slots:
    void bodyDestroyed();

private:
    QSharedPointer<btTypedConstraint> m_constraint;

    QSharedPointer<btRigidBody> m_rA, m_rB;

    QBullet::RigidBody *m_rigidBodyA;

    QBullet::RigidBody *m_rigidBodyB;
    QBullet::DiscreteDynamicsWorld *m_world;

    QSharedPointer<WorldData> m_worldData;

    bool m_disableCollisionsBetweenLinkedBodies;
};
}
#endif // CONSTRAINT_H
