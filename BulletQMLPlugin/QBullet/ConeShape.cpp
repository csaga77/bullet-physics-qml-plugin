/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "ConeShape.h"

namespace QBullet {

ConeShape::ConeShape(QObject *parent)
    : ConvexShape(parent)
    , m_radius(1.0)
    , m_height(1.0)
    , m_upAxis(Axis_Y)
{

}

QSharedPointer<btConeShape> ConeShape::coneShape() const
{
    return shape().dynamicCast<btConeShape>();
}

qreal ConeShape::radius() const
{
    return m_radius;
}

qreal ConeShape::height() const
{
    return m_height;
}

BulletObject::Axis ConeShape::upAxis() const
{
    return m_upAxis;
}

void ConeShape::setRadius(qreal radius)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_radius, radius))
        return;

    m_radius = radius;
    resetShape();
    emit radiusChanged(m_radius);
}

void ConeShape::setHeight(qreal height)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_height, height))
        return;

    m_height = height;
    resetShape();
    emit heightChanged(m_height);
}

void ConeShape::setUpAxis(BulletObject::Axis upAxis)
{
    if (m_upAxis == upAxis)
        return;

    m_upAxis = upAxis;
    resetShape();
    emit upAxisChanged(m_upAxis);
}

QSharedPointer<btCollisionShape> ConeShape::create() const
{
    switch(m_upAxis) {
    case Axis_X:
        return QSharedPointer<btCollisionShape>(new btConeShapeX(m_radius, m_height));
    case Axis_Z:
        return QSharedPointer<btCollisionShape>(new btConeShapeZ(m_radius, m_height));

    }
    return QSharedPointer<btCollisionShape>(new btConeShape(m_radius, m_height));
}

}
