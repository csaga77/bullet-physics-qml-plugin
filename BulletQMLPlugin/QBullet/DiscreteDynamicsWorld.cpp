#include <QtDebug>
#include <QDateTime>
#include <btBulletDynamicsCommon.h>

#include "../Stopwatch.h"
#include "CollisionObject.h"
#include "CollisionShape.h"
#include "ContactCallback.h"
#include "DiscreteDynamicsWorld.h"
#include "QBullet.h"
#include "RigidBody.h"

namespace QBullet {

void btInternalTickCallback(btDynamicsWorld *world, btScalar timeStep)
{
    DiscreteDynamicsWorld *w = (DiscreteDynamicsWorld *)world->getWorldUserInfo();
    Q_ASSERT(w);
    w->internalTickCallback(timeStep);
}

bool ContactProcessedCallback(
        btManifoldPoint &cp,
        void *body0,
        void *body1) {

    Q_ASSERT(body0&&body1);

    CollisionObject *objA = (CollisionObject *)((btCollisionObject *)body0)->getUserPointer();
    CollisionObject *objB = (CollisionObject *)((btCollisionObject *)body1)->getUserPointer();
    //objA and objB are assigned in CollisionObject::resetObject().
    Q_ASSERT(objA&&objB);
    QList<ContactCallback*> callbacks = objA->contactCallbacks(objB);
    foreach(ContactCallback *callback, callbacks) {
        callback->onContactProcessedCallback(cp, objA, objB);
    }
    callbacks = objA->contactCallbacks(0);
    foreach(ContactCallback *callback, callbacks) {
        callback->onContactProcessedCallback(cp, objA, objB);
    }
    callbacks = objB->contactCallbacks(objA);
    foreach(ContactCallback *callback, callbacks) {
        callback->onContactProcessedCallback(cp, objA, objB);
    }
    callbacks = objB->contactCallbacks(0);
    foreach(ContactCallback *callback, callbacks) {
        callback->onContactProcessedCallback(cp, objA, objB);
    }

    emit objA->collided(objB);
    emit objB->collided(objA);
    return true;
}

void ContactStartedCallback(btPersistentManifold* const &manifold)
{
}

void ContactEndedCallback(btPersistentManifold* const &manifold)
{

}

bool ContactDestroyedCallback(void* userPersistentData)
{
    return true;
}

/*!
Before ContactAddedCallback can be used, needs to set
CollisionObject::setCustomMaterialEnabled(true).
*/
bool ContactAddedCallback(btManifoldPoint& cp,
                          const btCollisionObjectWrapper* colObj0Wrap,
                          int partId0,
                          int index0,
                          const btCollisionObjectWrapper* colObj1Wrap,
                          int partId1,
                          int index1)
{
    CollisionObject *objA = (CollisionObject *)(colObj0Wrap->getCollisionObject())->getUserPointer();
    CollisionObject *objB = (CollisionObject *)(colObj1Wrap->getCollisionObject())->getUserPointer();
    //objA and objB are assigned in CollisionObject::resetObject().
    Q_ASSERT(objA&&objB);
    QList<ContactCallback*> callbacks = objA->contactCallbacks(objB);
    foreach(ContactCallback *callback, callbacks) {
        callback->onContactAddedCallback(cp, objA, partId0, index0,
                                         objB, partId1, index1);
    }
    callbacks = objA->contactCallbacks(0);
    foreach(ContactCallback *callback, callbacks) {
        callback->onContactAddedCallback(cp, objA, partId0, index0,
                                         objB, partId1, index1);
    }
    callbacks = objB->contactCallbacks(objA);
    foreach(ContactCallback *callback, callbacks) {
        callback->onContactAddedCallback(cp, objA, partId0, index0,
                                         objB, partId1, index1);
    }
    callbacks = objB->contactCallbacks(0);
    foreach(ContactCallback *callback, callbacks) {
        callback->onContactAddedCallback(cp, objA, partId0, index0,
                                         objB, partId1, index1);
    }

    return true;
}

DiscreteDynamicsWorld::DiscreteDynamicsWorld(QObject *parent)
    : BulletObject(parent)
    , m_data(new WorldData)
    , m_running(true)
    , m_gravity(0, -10, 0)
    , m_frames(0)
    , m_maxSubStep(1)
    , m_fps(60)
    , m_fixedTimeStep(1/60.0)
    , m_timer(0)
{
    m_data->dynamicsWorld->setGravity(q2b(m_gravity));
    m_data->dynamicsWorld->setWorldUserInfo(this);
    m_data->dynamicsWorld->setInternalTickCallback(btInternalTickCallback, this);
    gContactProcessedCallback = ContactProcessedCallback;
    gContactStartedCallback = ContactStartedCallback;
    gContactEndedCallback = ContactEndedCallback;
    gContactDestroyedCallback = ContactDestroyedCallback;
    gContactAddedCallback = ContactAddedCallback;
    m_lastTimeStamp = QDateTime::currentMSecsSinceEpoch();
    m_timer = startTimer(m_fixedTimeStep, Qt::PreciseTimer);
}

DiscreteDynamicsWorld::~DiscreteDynamicsWorld()
{

}

QSharedPointer<WorldData> DiscreteDynamicsWorld::worldData() const
{
    return m_data;
}

bool DiscreteDynamicsWorld::isRunning() const
{
    return m_running;
}

QVector3D DiscreteDynamicsWorld::gravity() const
{
    return m_gravity;
}

qint64 DiscreteDynamicsWorld::frames() const
{
    return m_frames;
}

void DiscreteDynamicsWorld::internalTickCallback(qreal timeStep)
{

}

int DiscreteDynamicsWorld::maxSubStep() const
{
    return m_maxSubStep;
}

qreal DiscreteDynamicsWorld::fps() const
{
    return m_fps;
}

qreal DiscreteDynamicsWorld::fixedTimeStep() const
{
    return m_fixedTimeStep;
}

void DiscreteDynamicsWorld::stepSimulation(qreal dt)
{
    if(!m_running)
        return;

    m_frames++;
    //qDebug()<<"DiscreteDynamicsWorld::stepSimulation()"<<dt;
    //Stopwatch stopwatch("DiscreteDynamicsWorld::stepSimulation()");

    m_data->dynamicsWorld->stepSimulation(dt*2, m_maxSubStep, m_fixedTimeStep);

}

void DiscreteDynamicsWorld::setRunning(bool running)
{
    if (m_running == running)
        return;

    m_running = running;
    emit runningChanged(m_running);
}

void DiscreteDynamicsWorld::setGravity(QVector3D gravity)
{
    if (m_gravity == gravity)
        return;

    m_gravity = gravity;
    m_data->dynamicsWorld->setGravity(q2b(m_gravity));
    emit gravityChanged(m_gravity);
}

void DiscreteDynamicsWorld::setMaxSubStep(int maxSubStep)
{
    if(maxSubStep<0)
        return;
    if (m_maxSubStep == maxSubStep)
        return;

    m_maxSubStep = maxSubStep;
    emit maxSubStepChanged(m_maxSubStep);
}

void DiscreteDynamicsWorld::setFps(qreal fps)
{
    if(fps<=0)
        return;
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_fps, fps))
        return;

    m_fps = fps;
    m_fixedTimeStep = 1/m_fps;
    emit fpsChanged(m_fps);
    emit fixedTimeStepChanged(m_fixedTimeStep);
    killTimer(m_timer);
    m_timer = startTimer(m_fixedTimeStep, Qt::PreciseTimer);
}

void DiscreteDynamicsWorld::timerEvent(QTimerEvent *)
{
    qint64 timeStamp = QDateTime::currentMSecsSinceEpoch();
    stepSimulation((timeStamp - m_lastTimeStamp)/1000.0);
    m_lastTimeStamp = timeStamp;
}

WorldData::WorldData()
    : dynamicsWorld(0)
    , broadphase(0)
    , collisionConfiguration(0)
    , dispatcher(0)
    , solver(0)
{
    broadphase = new btDbvtBroadphase();

    collisionConfiguration = new btDefaultCollisionConfiguration();

    dispatcher = new btCollisionDispatcher(collisionConfiguration);

    solver = new btSequentialImpulseConstraintSolver;

    dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
}

WorldData::~WorldData()
{
    delete dynamicsWorld;
    delete solver;
    delete collisionConfiguration;
    delete dispatcher;
    delete broadphase;
}

}

#include "moc_DiscreteDynamicsWorld.cpp"
