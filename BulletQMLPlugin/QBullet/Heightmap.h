/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef HEIGHTMAP_H
#define HEIGHTMAP_H

#include <QUrl>
#include <QSize>
#include <QVector3D>
#include "TriangleMesh.h"

namespace QBullet {

class HeightmapData: public QObject
{
    Q_OBJECT
public:
    HeightmapData(QUrl url, qreal heightValueScale = 1.0);

    ~HeightmapData();

    bool isValid() const;

    QRect rect() const;

    QUrl url() const;

    QSize size() const;

    int width() const;

    int height() const;

    const float *data() const;

    qreal heightValueScale() const;

    qreal minHeightValue() const;

    qreal maxHeightValue() const;

    qreal heightValue(int index) const;

    qreal heightValue(int row, int column) const;

    void setHeightValue(int index, qreal value);

    void setHeightValue(int row, int column, qreal value);

    int count() const;
private:
    QUrl m_url;
    QSize m_size;
    float *m_data;
    qreal m_heightValueScale;
    qreal m_minHeightValue;
    qreal m_maxHeightValue;
    int m_count;
};

class Heightmap: public TriangleMesh
{
    Q_OBJECT
    Q_PROPERTY(QUrl heightmap READ heightmap WRITE setHeightmap NOTIFY heightmapChanged)
    Q_PROPERTY(QVector3D scale READ scale WRITE setScale NOTIFY scaleChanged)

    Q_PROPERTY(bool diamondSubdivision READ diamondSubdivision WRITE setDiamondSubdivision NOTIFY diamondSubdivisionChanged)
    /*!
    If true, when value in height map is changed, it updates the triangle mesh
    vertex and emits changed signal.
    */
    Q_PROPERTY(bool autoUpdateMeshVertex READ autoUpdateMeshVertex WRITE setAutoUpdateMeshVertex NOTIFY autoUpdateMeshVertexChanged)

    Q_PROPERTY(qreal minHeightValue READ minHeightValue NOTIFY minHeightValueChanged)
    Q_PROPERTY(qreal maxHeightValue READ maxHeightValue NOTIFY maxHeightValueChanged)
    Q_PROPERTY(qreal halfHeightValue READ halfHeightValue NOTIFY halfHeightValueChanged)

    Q_PROPERTY(QSize size READ size NOTIFY sizeChanged)

    Q_PROPERTY(int width READ width NOTIFY sizeChanged)
    Q_PROPERTY(int height READ height NOTIFY sizeChanged)
public:
    Heightmap(QObject *parent = nullptr);

    QUrl heightmap() const;

    QVector3D scale() const;

    Q_INVOKABLE void testDynamicChange();

    Q_INVOKABLE void substract(const QVector3D &center, qreal radius);

    QRect toHeightmapRectXZ(const QVector3D &min, const QVector3D &max) const;

    QVector3D vertex(int index) const;

    QVector3D vertex(int row, int column) const;

    void setVertex(int index, const QVector3D &v);

    void setVertex(int row, int column, const QVector3D &v);

    void updateNormal(int row, int column);

    QVector3D normal(int index) const;

    QVector3D normal(int row, int column) const;

    void setNormal(int index, const QVector3D &n);

    void setNormal(int row, int column, const QVector3D &n);

    int index(int row, int column) const;

    bool find(QVector3D pos, int &row, int &column);

    bool autoUpdateMeshVertex() const;

    QSharedPointer<HeightmapData> heightmapData() const;

    bool diamondSubdivision() const;

    qreal minHeightValue() const;

    qreal maxHeightValue() const;

    qreal halfHeightValue() const;

    /*!
    Returns the height of the heightmap.

    It doesn't take the scale into accout, only returns the original height of
    the heightmap image.
    */
    int height() const;

    /*!
    Returns the width of the heightmap.

    It doesn't take the scale into accout, only returns the original width of
    the heightmap image.
    */
    int width() const;

    /*!
    Returns the size of the heightmap.

    It doesn't take the scale into accout, only returns the original size of
    the heightmap image.
    */
    QSize size() const;

public slots:
    void setHeightmap(QUrl heightmap);

    void setScale(QVector3D scale);

    void setAutoUpdateMeshVertex(bool autoUpdateMeshVertex);

    void setDiamondSubdivision(bool diamondSubdivision);

signals:
    void heightmapChanged(QUrl heightmap);

    void scaleChanged(QVector3D scale);

    void autoUpdateMeshVertexChanged(bool autoUpdateMeshVertex);

    void diamondSubdivisionChanged(bool diamondSubdivision);

    void minHeightValueChanged(qreal minHeightValue);

    void maxHeightValueChanged(qreal maxHeightValue);

    void halfHeightValueChanged(qreal halfHeightValue);

    void sizeChanged(QSize size);

protected:
    bool isATriangle(int a, int b, int c) const;

    void resetMesh();

    void onComponentComplete() override;

private:
    QUrl m_url;

    QVector3D m_scale;

    QSize m_heightmapSize;

    bool m_autoUpdateMeshVertex;

    QSharedPointer<HeightmapData> m_heightmapData;
    bool m_diamondSubdivision;
};

}

#endif // HEIGHTMAP_H
