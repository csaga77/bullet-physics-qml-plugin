/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "Generic6DofSpringConstraint.h"
#include "QBullet.h"

namespace QBullet
{

Generic6DofSpringConstraint::Generic6DofSpringConstraint(QObject *parent)
    : Generic6DofConstraint(parent)
    , m_linearSpringXEnabled(false)
    , m_linearSpringYEnabled(false)
    , m_linearSpringZEnabled(false)
    , m_angularSpringXEnabled(false)
    , m_angularSpringYEnabled(false)
    , m_angularSpringZEnabled(false)
{

}

QSharedPointer<btTypedConstraint> Generic6DofSpringConstraint::create() const
{
    if(rbB()) {
        if(rbA()) {
            return QSharedPointer<btTypedConstraint>(new btGeneric6DofSpringConstraint(*rbA().data(), *rbB().data(),
                                                                                       q2b(transformA()), q2b(transformB()),
                                                                                       false));
        } else {
            return QSharedPointer<btTypedConstraint>(new btGeneric6DofSpringConstraint(*rbB().data(), q2b(transformB()),
                                                                                       false));
        }
    }
    return QSharedPointer<btTypedConstraint>(0);
}

QSharedPointer<btGeneric6DofSpringConstraint> Generic6DofSpringConstraint::generic6DofSpring() const
{
    return constraint().dynamicCast<btGeneric6DofSpringConstraint>();
}

bool Generic6DofSpringConstraint::isLinearSpringXEnabled() const
{
    return m_linearSpringXEnabled;
}

bool Generic6DofSpringConstraint::isLinearSpringYEnabled() const
{
    return m_linearSpringYEnabled;
}

bool Generic6DofSpringConstraint::isLinearSpringZEnabled() const
{
    return m_linearSpringZEnabled;
}

bool Generic6DofSpringConstraint::isAngularSpringXEnabled() const
{
    return m_angularSpringXEnabled;
}

bool Generic6DofSpringConstraint::isAngularSpringYEnabled() const
{
    return m_angularSpringYEnabled;
}

bool Generic6DofSpringConstraint::isAngularSpringZEnabled() const
{
    return m_angularSpringZEnabled;
}

QVector3D Generic6DofSpringConstraint::linearStiffness() const
{
    return m_linearStiffness;
}

QVector3D Generic6DofSpringConstraint::angularStiffness() const
{
    return m_angularStiffness;
}

QVector3D Generic6DofSpringConstraint::linearDamping() const
{
    return m_linearDamping;
}

QVector3D Generic6DofSpringConstraint::angularDamping() const
{
    return m_angularDamping;
}


void Generic6DofSpringConstraint::setLinearSpringXEnabled(bool linearSpringXEnabled)
{
    if (m_linearSpringXEnabled == linearSpringXEnabled)
        return;

    m_linearSpringXEnabled = linearSpringXEnabled;
    if(generic6DofSpring()) {
        generic6DofSpring()->enableSpring(0, linearSpringXEnabled);
    }
    emit linearSpringXEnabledChanged(m_linearSpringXEnabled);
}

void Generic6DofSpringConstraint::setLinearSpringYEnabled(bool linearSpringYEnabled)
{
    if (m_linearSpringYEnabled == linearSpringYEnabled)
        return;

    m_linearSpringYEnabled = linearSpringYEnabled;
    if(generic6DofSpring()) {
        generic6DofSpring()->enableSpring(1, linearSpringYEnabled);
    }
    emit linearSpringYEnabledChanged(m_linearSpringYEnabled);
}

void Generic6DofSpringConstraint::setLinearSpringZEnabled(bool linearSpringZEnabled)
{
    if (m_linearSpringZEnabled == linearSpringZEnabled)
        return;

    m_linearSpringZEnabled = linearSpringZEnabled;
    if(generic6DofSpring()) {
        generic6DofSpring()->enableSpring(2, linearSpringZEnabled);
    }
    emit linearSpringZEnabledChanged(m_linearSpringZEnabled);
}

void Generic6DofSpringConstraint::setAngularSpringXEnabled(bool angularSpringXEnabled)
{
    if (m_angularSpringXEnabled == angularSpringXEnabled)
        return;

    m_angularSpringXEnabled = angularSpringXEnabled;
    if(generic6DofSpring()) {
        generic6DofSpring()->enableSpring(3, angularSpringXEnabled);
    }
    emit angularSpringXEnabledChanged(m_angularSpringXEnabled);
}

void Generic6DofSpringConstraint::setAngularSpringYEnabled(bool angularSpringYEnabled)
{
    if (m_angularSpringYEnabled == angularSpringYEnabled)
        return;

    m_angularSpringYEnabled = angularSpringYEnabled;
    if(generic6DofSpring()) {
        generic6DofSpring()->enableSpring(4, angularSpringYEnabled);
    }
    emit angularSpringYEnabledChanged(m_angularSpringYEnabled);
}

void Generic6DofSpringConstraint::setAngularSpringZEnabled(bool angularSpringZEnabled)
{
    if (m_angularSpringZEnabled == angularSpringZEnabled)
        return;

    m_angularSpringZEnabled = angularSpringZEnabled;
    if(generic6DofSpring()) {
        generic6DofSpring()->enableSpring(5, angularSpringZEnabled);
    }
    emit angularSpringZEnabledChanged(m_angularSpringZEnabled);
}

void Generic6DofSpringConstraint::setLinearStiffness(QVector3D linearStiffness)
{
    if (m_linearStiffness == linearStiffness)
        return;

    m_linearStiffness = linearStiffness;
    if(generic6DofSpring()) {
        generic6DofSpring()->setStiffness(0, linearStiffness.x());
        generic6DofSpring()->setStiffness(1, linearStiffness.y());
        generic6DofSpring()->setStiffness(2, linearStiffness.z());
    }
    emit linearStiffnessChanged(m_linearStiffness);
}

void Generic6DofSpringConstraint::setAngularStiffness(QVector3D angularStiffness)
{
    if (m_angularStiffness == angularStiffness)
        return;

    m_angularStiffness = angularStiffness;
    if(generic6DofSpring()) {
        generic6DofSpring()->setStiffness(3, angularStiffness.x());
        generic6DofSpring()->setStiffness(4, angularStiffness.y());
        generic6DofSpring()->setStiffness(5, angularStiffness.z());
    }
    emit angularStiffnessChanged(m_angularStiffness);
}

void Generic6DofSpringConstraint::setLinearDamping(QVector3D linearDamping)
{
    if (m_linearDamping == linearDamping)
        return;

    m_linearDamping = linearDamping;
    if(generic6DofSpring()) {
        generic6DofSpring()->setDamping(0, linearDamping.x());
        generic6DofSpring()->setDamping(1, linearDamping.y());
        generic6DofSpring()->setDamping(2, linearDamping.z());
    }
    emit linearDampingChanged(m_linearDamping);
}

void Generic6DofSpringConstraint::setAngularDamping(QVector3D angularDamping)
{
    if (m_angularDamping == angularDamping)
        return;

    m_angularDamping = angularDamping;
    if(generic6DofSpring()) {
        generic6DofSpring()->setDamping(3, angularDamping.x());
        generic6DofSpring()->setDamping(4, angularDamping.y());
        generic6DofSpring()->setDamping(5, angularDamping.z());
    }
    emit angularDampingChanged(m_angularDamping);
}

void Generic6DofSpringConstraint::postCreate()
{
    Generic6DofConstraint::postCreate();
    if(generic6Dof()) {
        generic6DofSpring()->enableSpring(0, m_linearSpringXEnabled);
        generic6DofSpring()->enableSpring(1, m_linearSpringYEnabled);
        generic6DofSpring()->enableSpring(2, m_linearSpringZEnabled);
        generic6DofSpring()->enableSpring(3, m_angularSpringXEnabled);
        generic6DofSpring()->enableSpring(4, m_angularSpringYEnabled);
        generic6DofSpring()->enableSpring(5, m_angularSpringZEnabled);

        generic6DofSpring()->setStiffness(0, m_linearStiffness.x());
        generic6DofSpring()->setStiffness(1, m_linearStiffness.y());
        generic6DofSpring()->setStiffness(2, m_linearStiffness.z());
        generic6DofSpring()->setStiffness(3, m_angularStiffness.x());
        generic6DofSpring()->setStiffness(4, m_angularStiffness.y());
        generic6DofSpring()->setStiffness(5, m_angularStiffness.z());

        generic6DofSpring()->setDamping(0, m_linearDamping.x());
        generic6DofSpring()->setDamping(1, m_linearDamping.y());
        generic6DofSpring()->setDamping(2, m_linearDamping.z());
        generic6DofSpring()->setDamping(3, m_angularDamping.x());
        generic6DofSpring()->setDamping(4, m_angularDamping.y());
        generic6DofSpring()->setDamping(5, m_angularDamping.z());
    }

}


}
