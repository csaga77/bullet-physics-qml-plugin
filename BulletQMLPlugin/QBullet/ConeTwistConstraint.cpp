/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "ConeTwistConstraint.h"
#include "QBullet.h"

namespace QBullet
{

ConeTwistConstraint::ConeTwistConstraint(QObject *parent)
    : Constraint(parent)
    , m_swingSpan1(90)
    , m_swingSpan2(90)
    , m_twistSpan(90)
    , m_softness(1.0)
    , m_biasFactor(0.3)
    , m_relaxationFactor(1.0)
    , m_angularOnly(false)
    , m_damping(0)
{

}

QMatrix4x4 ConeTwistConstraint::transformA() const
{
    QMatrix4x4 mat;
    mat.translate(m_pivotA);
    mat.rotate(m_rotA);
    return mat;
}

QMatrix4x4 ConeTwistConstraint::transformB() const
{
    QMatrix4x4 mat;
    mat.translate(m_pivotB);
    mat.rotate(m_rotB);
    return mat;
}

qreal ConeTwistConstraint::coneSpan1() const
{
    return m_swingSpan1;
}

qreal ConeTwistConstraint::coneSpan2() const
{
    return m_swingSpan2;
}

qreal ConeTwistConstraint::twistSpan() const
{
    return m_twistSpan;
}

qreal ConeTwistConstraint::softness() const
{
    return m_softness;
}

qreal ConeTwistConstraint::biasFactor() const
{
    return m_biasFactor;
}

qreal ConeTwistConstraint::relaxationFactor() const
{
    return m_relaxationFactor;
}

bool ConeTwistConstraint::angularOnly() const
{
    return m_angularOnly;
}

qreal ConeTwistConstraint::damping() const
{
    return m_damping;
}

void ConeTwistConstraint::setConeSpan1(qreal swingSpan1)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_swingSpan1, swingSpan1))
        return;

    m_swingSpan1 = swingSpan1;
    updateLimit();
    emit coneSpan1Changed(m_swingSpan1);
}

void ConeTwistConstraint::setConeSpan2(qreal swingSpan2)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_swingSpan2, swingSpan2))
        return;

    m_swingSpan2 = swingSpan2;
    updateLimit();
    emit coneSpan2Changed(m_swingSpan2);
}

void ConeTwistConstraint::setTwistSpan(qreal twistSpan)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_twistSpan, twistSpan))
        return;

    m_twistSpan = twistSpan;
    updateLimit();
    emit twistSpanChanged(m_twistSpan);
}

void ConeTwistConstraint::setSoftness(qreal softness)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_softness, softness))
        return;

    m_softness = softness;
    updateLimit();
    emit softnessChanged(m_softness);
}

void ConeTwistConstraint::setBiasFactor(qreal biasFactor)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_biasFactor, biasFactor))
        return;

    m_biasFactor = biasFactor;
    updateLimit();
    emit biasFactorChanged(m_biasFactor);
}

void ConeTwistConstraint::setRelaxationFactor(qreal relaxationFactor)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_relaxationFactor, relaxationFactor))
        return;

    m_relaxationFactor = relaxationFactor;
    updateLimit();
    emit relaxationFactorChanged(m_relaxationFactor);
}

void ConeTwistConstraint::setAngularOnly(bool angularOnly)
{
    if (m_angularOnly == angularOnly)
        return;

    m_angularOnly = angularOnly;
    if(coneTwist()) {
        coneTwist()->setAngularOnly(angularOnly);
    }
    emit angularOnlyChanged(m_angularOnly);
}

void ConeTwistConstraint::setDamping(qreal damping)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_damping, damping))
        return;

    m_damping = damping;
    coneTwist()->setDamping(m_damping);
    emit dampingChanged(m_damping);
}

QSharedPointer<btTypedConstraint> ConeTwistConstraint::create() const
{
    if(rbA()) {
        if(rbB()) {
            return QSharedPointer<btTypedConstraint>(new btConeTwistConstraint(*rbA().data(), *rbB().data(), q2b(transformA()), q2b(transformB())));
        } else {
            return QSharedPointer<btTypedConstraint>(new btConeTwistConstraint(*rbA().data(), q2b(transformA())));
        }
    }
    return QSharedPointer<btTypedConstraint>(0);
}

void ConeTwistConstraint::postCreate()
{
    Constraint::postCreate();
    updateLimit();
    if(coneTwist()) {

        coneTwist()->setAngularOnly(m_angularOnly);

        coneTwist()->setDamping(m_damping);
    }
}

QSharedPointer<btConeTwistConstraint> ConeTwistConstraint::coneTwist() const
{
    return constraint().dynamicCast<btConeTwistConstraint>();
}

void ConeTwistConstraint::updateLimit()
{
    if(coneTwist()) {
        coneTwist()->setLimit(qDegreesToRadians(m_swingSpan1), qDegreesToRadians(m_swingSpan2), qDegreesToRadians(m_twistSpan),
                              m_softness, m_biasFactor, m_relaxationFactor);
    }
}

QVector3D ConeTwistConstraint::pivotA() const
{
    return m_pivotA;
}

QVector3D ConeTwistConstraint::pivotB() const
{
    return m_pivotB;
}

QQuaternion ConeTwistConstraint::rotationA() const
{
    return m_rotA;
}

QQuaternion ConeTwistConstraint::rotationB() const
{
    return m_rotB;
}

void ConeTwistConstraint::setPivotA(QVector3D pivotA)
{
    if (m_pivotA == pivotA)
        return;

    m_pivotA = pivotA;
    if(coneTwist()) {
        coneTwist()->setFrames(q2b(transformA()), q2b(transformB()));
    }
    emit pivotAChanged(m_pivotA);
}

void ConeTwistConstraint::setPivotB(QVector3D pivotB)
{
    if (m_pivotB == pivotB)
        return;

    m_pivotB = pivotB;
    if(coneTwist()) {
        coneTwist()->setFrames(q2b(transformA()), q2b(transformB()));
    }
    emit pivotBChanged(m_pivotB);
}

void ConeTwistConstraint::setRotationA(QQuaternion rotA)
{
    if(m_rotA==rotA)
        return;
    m_rotA = rotA;
    if(coneTwist()) {
        coneTwist()->setFrames(q2b(transformA()), q2b(transformB()));
    }
    emit rotationAChanged(rotA);
}

void ConeTwistConstraint::setRotationB(QQuaternion rotB)
{
    if(m_rotB==rotB)
        return;
    m_rotB = rotB;
    if(coneTwist()) {
        coneTwist()->setFrames(q2b(transformA()), q2b(transformB()));
    }
    emit rotationAChanged(rotB);
}


qreal ConeTwistConstraint::yawA() const
{
    return m_rotA.toEulerAngles().y();
}

qreal ConeTwistConstraint::pitchA() const
{
    return m_rotA.toEulerAngles().x();
}

qreal ConeTwistConstraint::rollA() const
{
    return m_rotA.toEulerAngles().z();
}

qreal ConeTwistConstraint::yawB() const
{
    return m_rotB.toEulerAngles().y();
}

qreal ConeTwistConstraint::pitchB() const
{
    return m_rotB.toEulerAngles().x();
}

qreal ConeTwistConstraint::rollB() const
{
    return m_rotB.toEulerAngles().z();
}

void ConeTwistConstraint::setYawA(qreal yaw)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->yawA(), yaw))
        return;

    setRotationA(QQuaternion::fromEulerAngles(pitchA(), yaw, rollA()));
    emit yawAChanged(yaw);
}

void ConeTwistConstraint::setPitchA(qreal pitch)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->pitchA(), pitch))
        return;

    setRotationA(QQuaternion::fromEulerAngles(pitch, yawA(), rollA()));
    emit pitchAChanged(pitch);
}

void ConeTwistConstraint::setRollA(qreal roll)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->rollA(), roll))
        return;

    setRotationA(QQuaternion::fromEulerAngles(pitchA(), yawA(), roll));
    emit rollAChanged(roll);
}

void ConeTwistConstraint::setYawB(qreal yaw)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->yawB(), yaw))
        return;

    setRotationB(QQuaternion::fromEulerAngles(pitchB(), yaw, rollB()));
    emit yawBChanged(yaw);
}

void ConeTwistConstraint::setPitchB(qreal pitch)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->pitchB(), pitch))
        return;

    setRotationB(QQuaternion::fromEulerAngles(pitch, yawB(), rollB()));
    emit pitchBChanged(pitch);
}

void ConeTwistConstraint::setRollB(qreal roll)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->rollB(), roll))
        return;

    setRotationB(QQuaternion::fromEulerAngles(pitchB(), yawB(), roll));
    emit rollBChanged(roll);
}


}
