/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef HEIGHTMAPMODIFIER_H
#define HEIGHTMAPMODIFIER_H

#include <QVector3D>
#include "BulletObject.h"

namespace QBullet {

class CollisionObject;
class HeightmapModifier : public BulletObject
{
    Q_OBJECT
    Q_PROPERTY(QBullet::CollisionObject* heightmapObject READ heightmapObject WRITE setHeightmapObject NOTIFY heightmapObjectChanged)
    Q_PROPERTY(QBullet::CollisionObject* collisionObject READ collisionObject WRITE setCollisionObject NOTIFY collisionObjectChanged)
    Q_PROPERTY(QVector3D offset READ offset WRITE setOffset NOTIFY offsetChanged)
public:
    explicit HeightmapModifier(QObject *parent = nullptr);

    QBullet::CollisionObject *heightmapObject() const;

    QBullet::CollisionObject *collisionObject() const;

    QVector3D offset() const;

signals:

    void heightmapObjectChanged(QBullet::CollisionObject *heightmapObject);

    void collisionObjectChanged(QBullet::CollisionObject *collisionObject);

    void offsetChanged(QVector3D offset);

public slots:
    void setHeightmapObject(QBullet::CollisionObject *heightmapObject);

    void setCollisionObject(QBullet::CollisionObject *collisionObject);

    void setOffset(QVector3D offset);

    void substract();

private:
    QBullet::CollisionObject *m_heightmapObject;

    QBullet::CollisionObject *m_collisionObject;

    QVector3D m_offset;
};

}

#endif // HEIGHTMAPMODIFIER_H
