/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef AUTOHEIGHTMAPIMPACTMODIFIER_H
#define AUTOHEIGHTMAPIMPACTMODIFIER_H

#include "ContactCallback.h"

namespace QBullet {

class AutoHeightmapImpactModifier : public ContactCallback
{
    Q_OBJECT
    Q_PROPERTY(qreal modifierScale READ modifierScale WRITE setModifierScale NOTIFY modifierScaleChanged)
    Q_PROPERTY(qreal impulsiveThreshold READ impulsiveThreshold WRITE setImpulsiveThreshold NOTIFY impulsiveThresholdChanged)
public:
    explicit AutoHeightmapImpactModifier(QObject *parent = nullptr);

    bool onContactProcessedCallback (btManifoldPoint &cp,
                                     CollisionObject *objA,
                                     CollisionObject *objB) override;

    bool onContactAddedCallback(btManifoldPoint& cp,
                                        const CollisionObject* objA,
                                        int partId0,
                                        int index0,
                                        const CollisionObject* objB,
                                        int partId1,
                                        int index1) override;

    qreal modifierScale() const;

    qreal impulsiveThreshold() const;

signals:

    void modifierScaleChanged(qreal modifierScale);

    void impulsiveThresholdChanged(qreal impulsiveThreshold);

public slots:
    void setModifierScale(qreal modifierScale);

    void setImpulsiveThreshold(qreal impulsiveThreshold);

private:
    qreal m_modifierScale;
    qreal m_impulsiveThreshold;
};

}

#endif // AUTOHEIGHTMAPIMPACTMODIFIER_H
