/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <QtDebug>
#include <btBulletDynamicsCommon.h>
#include "CollisionObject.h"
#include "CollisionShape.h"
#include "ContactCallback.h"
#include "DiscreteDynamicsWorld.h"
#include "QBullet.h"

namespace QBullet {

CollisionObject::CollisionObject(QObject *parent)
    : BulletObject(parent)
    , m_collisionShape(0)
    , m_world(0)
    , m_collisionFilterSet(false)
    , m_collisionFilterGroup(0)
    , m_collisionFilterMask(0)
    , m_isCustomMaterialEnabled(false)
    , m_frames(0)
{

}

QSharedPointer<btCollisionObject> CollisionObject::collisionObject() const
{
    return m_btCollisionObject;
}

CollisionShape *CollisionObject::collisionShape() const
{
    return m_collisionShape;
}

DiscreteDynamicsWorld *CollisionObject::world() const
{
    return m_world;
}


void CollisionObject::setCollisionShape(CollisionShape *collisionShape)
{
    if (m_collisionShape == collisionShape)
        return;
    takeFromWorld();
    if(m_collisionShape)
        m_collisionShape->disconnect(this);
    m_collisionShape = collisionShape;
    if(m_collisionShape) {
        connect(m_collisionShape, &CollisionShape::shapeReset, this, &CollisionObject::resetCollisionShape);
        connect(m_collisionShape, &CollisionShape::propertyChanged, this, &CollisionObject::collisionShapePropertyChanged);
    }
    resetCollisionShape();
    emit collisionShapeChanged(m_collisionShape);
}

void CollisionObject::setWorld(DiscreteDynamicsWorld *world)
{
    if (m_world == world)
        return;
    takeFromWorld();
    m_worldData.clear();
    m_world = world;

    if(m_world) {
        m_worldData = m_world->worldData();
        connect(m_world, &DiscreteDynamicsWorld::gravityChanged, this, &CollisionObject::worldGravityChanged);
    }
    onWorldChanged();
    addToWorld();
    emit worldChanged(m_world);
}


int CollisionObject::collisionFilterGroup() const
{
    return m_collisionFilterGroup;
}

int CollisionObject::collisionFilterMask() const
{
    return m_collisionFilterMask;
}

bool CollisionObject::isCustomMaterialEnabled() const
{
    return m_isCustomMaterialEnabled;
}

QList<ContactCallback *> CollisionObject::contactCallbacks(CollisionObject *object0) const
{
    return m_contactCallbacks[object0];
}

bool CollisionObject::findClosestPoint(const QVector3D &from, const QVector3D &rayVector, QVector3D &result) const
{
    if(m_worldData.isNull()||
            m_btCollisionObject.isNull()||
            m_btCollisionShape.isNull()) {
        qDebug()<<"CollisionObject::findClosestPoint() failed";
        return false;
    }

    btCollisionWorld::ClosestRayResultCallback rayCallback(q2b(from), q2b(from + rayVector*10));
    btTransform btFrom, btTo;
    btFrom.setIdentity();
    btTo.setIdentity();
    btFrom.setOrigin(rayCallback.m_rayFromWorld);
    btTo.setOrigin(rayCallback.m_rayToWorld);
    m_worldData->dynamicsWorld->rayTestSingle(btFrom, btTo,
                                              m_btCollisionObject.data(),
                                              m_btCollisionShape.data(),
                                              m_btCollisionObject->getWorldTransform(),
                                              rayCallback);
    result = b2q(rayCallback.m_hitPointWorld);
    return rayCallback.hasHit();
}

void CollisionObject::aabb(QVector3D &min, QVector3D &max) const
{
    if(m_btCollisionObject&&m_btCollisionShape) {
        btVector3 btMin, btMax;
        m_btCollisionShape->getAabb(m_btCollisionObject->getWorldTransform(), btMin, btMax);
        min = b2q(btMin);
        max = b2q(btMax);
    }
}

void CollisionObject::setCollisionFilterGroup(int group)
{
    if (m_collisionFilterGroup == group)
        return;
    takeFromWorld();
    m_collisionFilterSet = true;
    m_collisionFilterGroup = group;
    addToWorld();
    emit collisionFilterGroupChanged(m_collisionFilterGroup);
}

void CollisionObject::setCollisionFilterMask(int mask)
{
    if (m_collisionFilterMask == mask)
        return;

    takeFromWorld();
    m_collisionFilterSet = true;
    m_collisionFilterMask = mask;
    addToWorld();
    emit collisionFilterMaskChanged(m_collisionFilterMask);
}

void CollisionObject::setCustomMaterialEnabled(bool enabled)
{
    if(m_isCustomMaterialEnabled==enabled)
        return;
    m_isCustomMaterialEnabled = enabled;
    if(m_btCollisionObject) {
        if(m_isCustomMaterialEnabled) {
            m_btCollisionObject->setCollisionFlags(m_btCollisionObject->getCollisionFlags() |
                                                   btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);
        } else {
            m_btCollisionObject->setCollisionFlags(m_btCollisionObject->getCollisionFlags() &
                                                   (~btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK));
        }
    }
    emit customMaterialEnabledChanged(enabled);
}
void CollisionObject::resetObject()
{
    if(!isComplete())
        return;
    takeFromWorld();
    if(m_btCollisionObject) {
        m_btCollisionObject->setCollisionShape(0);
    }
    m_btCollisionObject = create();
    if(m_btCollisionObject) {
        m_btCollisionObject->setUserPointer(this);
        postCreate();
        addToWorld();
    }
    emit objectReset();
}

void CollisionObject::takeFromWorld()
{
    if(m_btCollisionObject.isNull())
        return;
    if(m_worldData) {
        m_worldData->dynamicsWorld->removeCollisionObject(m_btCollisionObject.data());
    }
}

void CollisionObject::addToWorld()
{
    if(m_worldData&&m_btCollisionObject) {
        if(m_collisionFilterSet) {
            onAddToWorld(m_worldData->dynamicsWorld,
                         m_collisionFilterSet,
                         m_collisionFilterGroup,
                         m_collisionFilterMask);
        } else {
            onAddToWorld(m_worldData->dynamicsWorld,
                         false,
                         btBroadphaseProxy::StaticFilter,
                         btBroadphaseProxy::AllFilter ^ btBroadphaseProxy::StaticFilter);
        }

        //        There are five states a rigid body could be in:

        //        ACTIVE_TAG
        //        Means active so that the object having the state could be moved in a step simulation. This is the "normal" state for an object to be in. Use btCollisionObject::activate() to activate an object, not btCollisionObject::setActivationState(ACTIVATE_TAG), or it may get disabled again right away, as the deactivation timer has not been reset.
        //        DISABLE_DEACTIVATION
        //        Makes a body active forever, used for something like a player-controlled object.
        //        DISABLE_SIMULATION
        //        Does the opposite, making a body deactivated forever.
        //        ISLAND_SLEEPING
        //        Means the body, and it's island, are asleep, since Bullet sleeps objects per-island. You probably don't want or need to set this one manually.
        //        WANTS_DEACTIVATION
        //        Means that it's an active object trying to fall asleep, and Bullet is keeping an eye on its velocity for the next few frames to see if it's a good candidate. You probably don't want or need to set this one manually.
        if(!m_btCollisionObject->isStaticObject()) {
            m_btCollisionObject->setActivationState(DISABLE_DEACTIVATION);
        } else {
            m_btCollisionObject->forceActivationState(WANTS_DEACTIVATION);
        }
    }
}

void CollisionObject::resetCollisionShape()
{
    takeFromWorld();
    m_btCollisionShape.clear();
    if(m_collisionShape) {
        m_btCollisionShape = m_collisionShape->shape();
    }
    if(m_btCollisionObject) {
        m_btCollisionObject->setCollisionShape(m_btCollisionShape.data());
    }
    onCollisionShapeReset();
    addToWorld();
}

void CollisionObject::collisionShapePropertyChanged()
{
    onCollisionShapePropertyChanged();
}

void CollisionObject::worldGravityChanged()
{
    onWorldGravityChanged();
}

void CollisionObject::onOriginChanged(const QVector3D &origin)
{

}

void CollisionObject::onRotationChanged(const QQuaternion &rotation)
{

}

void CollisionObject::onAddToWorld(btDiscreteDynamicsWorld *world,
                                   bool hasFilter,
                                   int collisionFilterGroup,
                                   int collisionFilterMask)
{
    if(hasFilter) {
        world->addCollisionObject(m_btCollisionObject.data(), collisionFilterGroup, collisionFilterMask);
    } else {
        world->addCollisionObject(m_btCollisionObject.data());
    }
    //        There are five states a rigid body could be in:

    //        ACTIVE_TAG
    //        Means active so that the object having the state could be moved in a step simulation. This is the "normal" state for an object to be in. Use btCollisionObject::activate() to activate an object, not btCollisionObject::setActivationState(ACTIVATE_TAG), or it may get disabled again right away, as the deactivation timer has not been reset.
    //        DISABLE_DEACTIVATION
    //        Makes a body active forever, used for something like a player-controlled object.
    //        DISABLE_SIMULATION
    //        Does the opposite, making a body deactivated forever.
    //        ISLAND_SLEEPING
    //        Means the body, and it's island, are asleep, since Bullet sleeps objects per-island. You probably don't want or need to set this one manually.
    //        WANTS_DEACTIVATION
    //        Means that it's an active object trying to fall asleep, and Bullet is keeping an eye on its velocity for the next few frames to see if it's a good candidate. You probably don't want or need to set this one manually.
    if(!m_btCollisionObject->isStaticObject()) {
        m_btCollisionObject->setActivationState(DISABLE_DEACTIVATION);
    } else {
        m_btCollisionObject->forceActivationState(WANTS_DEACTIVATION);
    }
}

void CollisionObject::onComponentComplete()
{
    if(!isComplete())
        return;
    resetObject();
}

void CollisionObject::onCollisionShapePropertyChanged()
{

}

void CollisionObject::onCollisionShapeReset()
{

}

void CollisionObject::onWorldGravityChanged()
{

}

void CollisionObject::onWorldChanged()
{

}

QSharedPointer<btCollisionShape> CollisionObject::bulletCollisionShape() const
{
    return m_btCollisionShape;
}

void CollisionObject::postCreate()
{
    if(m_btCollisionObject.isNull())
        return;
    if(m_isCustomMaterialEnabled) {
        m_btCollisionObject->setCollisionFlags(m_btCollisionObject->getCollisionFlags() |
                                               btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);
    } else {
        m_btCollisionObject->setCollisionFlags(m_btCollisionObject->getCollisionFlags() &
                                               (~btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK));
    }
    //Initial transform
    btTransform trans;
    trans.setIdentity();
    trans.setOrigin(q2b(m_origin));
    trans.setRotation(q2b(m_rotation));
    m_btCollisionObject->setWorldTransform(trans);
}

void CollisionObject::addCallback(ContactCallback *callback)
{
    if(m_contactCallbacks[callback->object1()].contains(callback)) {
        qWarning()<<"DiscreteDynamicsWorld::addCallback() callback already exists!";
        return;
    }
    m_contactCallbacks[callback->object1()]<<callback;
}

void CollisionObject::removeCallback(ContactCallback *callback)
{
    m_contactCallbacks[callback->object1()].removeOne(callback);
}

QMatrix4x4 CollisionObject::matrix() const
{
    return m_matrix;
}

QVector3D CollisionObject::origin() const
{
    return m_origin;
}

QQuaternion CollisionObject::rotation() const
{
    return m_rotation;
}

qreal CollisionObject::yaw() const
{
    return rotation().toEulerAngles().y();
}

qreal CollisionObject::pitch() const
{
    return rotation().toEulerAngles().x();
}

qreal CollisionObject::roll() const
{
    return rotation().toEulerAngles().z();
}

QVector3D CollisionObject::world2Local(const QVector3D &point) const
{
    //return b2q(body()->getWorldTransform().inverse() * q2b(point));
    QMatrix4x4 mat = matrix().inverted();
    return mat * point;
}

QVector3D CollisionObject::local2World(const QVector3D &point) const
{
    //return b2q(body()->getWorldTransform() * q2b(point));
    return matrix() * point;
}

QVector3D CollisionObject::xAxis() const
{
    return rotation()*QVector3D(1, 0, 0);
}

QVector3D CollisionObject::yAxis() const
{
    return rotation()*QVector3D(0, 1, 0);
}

QVector3D CollisionObject::zAxis() const
{
    return rotation()*QVector3D(0, 0, 1);
}

void CollisionObject::setOrigin(QVector3D origin)
{
    if (m_origin == origin)
        return;
    m_origin = origin;
    QMatrix4x4 oldMat = matrix();
    if(m_btCollisionObject) {
        btTransform trans = m_btCollisionObject->getWorldTransform();
        trans.setOrigin(q2b(origin));
        /*
        The MotionState will call RigidBody::setTransform() which will emit
        originChanged signal.
        */
        m_btCollisionObject->setWorldTransform(trans);
        onOriginChanged(origin);
    }
    emit originChanged(m_origin);
    if(oldMat!=matrix())
        emit matrixChanged(matrix());
}

void CollisionObject::setRotation(QQuaternion rotation)
{
    if (m_rotation == rotation)
        return;

    m_rotation = rotation;
    QMatrix4x4 oldMat = matrix();

    if(m_btCollisionObject) {
        btTransform trans = m_btCollisionObject->getWorldTransform();
        trans.setRotation(q2b(rotation));
        /*
        The MotionState will call RigidBody::setTransform() which will emit
        originChanged signal.
        */
        m_btCollisionObject->setWorldTransform(trans);
        onRotationChanged(m_rotation);
    }
    emit rotationChanged(rotation);
    if(oldMat!=matrix())
        emit matrixChanged(matrix());
}

void CollisionObject::setYaw(qreal yaw)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->yaw(), yaw))
        return;

    setRotation(QQuaternion::fromEulerAngles(pitch(), yaw, roll()));

}

void CollisionObject::setPitch(qreal pitch)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->pitch(), pitch))
        return;

    //qDebug()<<"RigidBody()"<<pitch;
    setRotation(QQuaternion::fromEulerAngles(pitch, yaw(), roll()));

}

void CollisionObject::setRoll(qreal roll)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->roll(), roll))
        return;

    setRotation(QQuaternion::fromEulerAngles(pitch(), yaw(), roll));

}

bool CollisionObject::updateFromBulletEngine(const btTransform &mat)
{
    if(m_btCollisionObject.isNull())
        return false;

    //if(m_frames==world()->frames())
    //    return;
    m_frames = world()?world()->frames():0;

    QMatrix4x4 qmat = b2q(mat);
    if(m_matrix!=qmat) {
        m_matrix = qmat;
        emit matrixChanged(qmat);
    }
    QVector3D origin = b2q(mat.getOrigin());
    if(origin!=m_origin) {
        m_origin = origin;
        emit originChanged(m_origin);
    }

    QQuaternion rotation = b2q(mat.getRotation());
    if(rotation!=m_rotation) {
        m_rotation = rotation;
        emit rotationChanged(rotation);
    }
    return true;
}

}
