/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef CAPSULESHAPE_H
#define CAPSULESHAPE_H

#include <QVector3D>
#include "ConvexShape.h"
class btCapsuleShape;

namespace QBullet {

class CapsuleShape : public ConvexShape
{
    Q_OBJECT
    Q_PROPERTY(qreal radius READ radius WRITE setRadius NOTIFY radiusChanged)
    Q_PROPERTY(qreal height READ height WRITE setHeight NOTIFY heightChanged)
    Q_PROPERTY(qreal totalHeight READ totalHeight WRITE setTotalHeight NOTIFY totalHeightChanged)
    Q_PROPERTY(BulletObject::Axis upAxis READ upAxis WRITE setUpAxis NOTIFY upAxisChanged)
public:
    explicit CapsuleShape(QObject *parent = nullptr);

    QSharedPointer<btCapsuleShape> capsuleShape() const;

    qreal radius() const;

    qreal height() const;

    qreal totalHeight() const;

    BulletObject::Axis upAxis() const;

signals:
    void radiusChanged(qreal radius);

    void heightChanged(qreal height);

    void totalHeightChanged(qreal totalHeight);

    void upAxisChanged(BulletObject::Axis);

public slots:

    void setRadius(qreal radius);

    void setHeight(qreal height);

    void setTotalHeight(qreal totalHeight);

    void setUpAxis(BulletObject::Axis axis);

protected:
    QSharedPointer<btCollisionShape> create() const override;

private:
    qreal m_radius;

    qreal m_height;

    BulletObject::Axis m_upAxis;
};

}

#endif // CAPSULESHAPE_H
