/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "QBullet.h"
#include "SliderConstraint.h"

namespace QBullet {

SliderConstraint::SliderConstraint(QObject *parent)
    : Constraint(parent)
{

}

QSharedPointer<btSliderConstraint> SliderConstraint::slider() const
{
    return constraint().dynamicCast<btSliderConstraint>();
}

QSharedPointer<btTypedConstraint> SliderConstraint::create() const
{
    QSharedPointer<btTypedConstraint> slider;

    if(rbA() && rbB()) {
        slider.reset(new btSliderConstraint(*rbA().data(), *rbB().data(), rbA()->getWorldTransform(), rbB()->getWorldTransform(), false));
    } else if(rbB()) {
        slider.reset(new btSliderConstraint(*rbB().data(), rbB()->getWorldTransform(), false));
    }

    return slider;
}

}
