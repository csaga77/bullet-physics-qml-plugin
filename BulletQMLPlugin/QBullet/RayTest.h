/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef RAYTEST_H
#define RAYTEST_H

#include <QObject>
#include <QSharedPointer>
#include <QVector3D>
namespace QBullet {

class DiscreteDynamicsWorld;
class RigidBody;
class WorldData;

class RayTest : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QBullet::DiscreteDynamicsWorld* world READ world WRITE setWorld NOTIFY worldChanged)
    Q_PROPERTY(QBullet::RigidBody* hitBody READ hitBody NOTIFY hitBodyChanged)
    Q_PROPERTY(QVector3D rayFrom READ rayFrom WRITE setRayFrom NOTIFY rayFromChanged)
    Q_PROPERTY(QVector3D rayTo READ rayTo WRITE setRayTo NOTIFY rayToChanged)
    Q_PROPERTY(QVector3D rayVector READ rayVector WRITE setRayVector NOTIFY rayVectorChanged)
    Q_PROPERTY(QVector3D hitPosition READ hitPosition NOTIFY hitPositionChanged)
    Q_PROPERTY(bool hasHit READ hasHit NOTIFY hasHitChanged)

    Q_PROPERTY(int collisionFilterGroup READ collisionFilterGroup WRITE setCollisionFilterGroup NOTIFY collisionFilterGroupChanged)
    Q_PROPERTY(int collisionFilterMask READ collisionFilterMask WRITE setCollisionFilterMask NOTIFY collisionFilterMaskChanged)
public:
    explicit RayTest(QObject *parent = nullptr);

    QBullet::DiscreteDynamicsWorld *world() const;

    QBullet::RigidBody *hitBody() const;

    QVector3D rayFrom() const;

    QVector3D rayTo() const;

    QVector3D hitPosition() const;

    QVector3D rayVector() const;

    bool hasHit() const;

    int collisionFilterGroup() const;

    int collisionFilterMask() const;

signals:
    void worldChanged(QBullet::DiscreteDynamicsWorld *world);

    void hitBodyChanged(QBullet::RigidBody *hitBody);

    void rayFromChanged(QVector3D rayFrom);

    void rayToChanged(QVector3D rayTo);

    void hitPositionChanged(QVector3D hitPosition);

    void rayVectorChanged(QVector3D rayVector);

    void hasHitChanged(bool hasHit);

    void collisionFilterGroupChanged(int collisionFilterGroup);

    void collisionFilterMaskChanged(int collisionFilterMask);

public slots:
    void setWorld(QBullet::DiscreteDynamicsWorld* world);

    void hitTest();

    void clear();

    void setRayFrom(QVector3D rayFrom);

    void setRayTo(QVector3D rayTo);

    void setRayVector(QVector3D rayVector);

    void setCollisionFilterGroup(int collisionFilterGroup);

    void setCollisionFilterMask(int collisionFilterMask);

private:
    QBullet::DiscreteDynamicsWorld* m_world;
    QSharedPointer<WorldData> m_worldData;
    QBullet::RigidBody *m_hitBody;
    QVector3D m_rayFrom;
    QVector3D m_rayTo;
    QVector3D m_hitPosition;
    bool m_hasHit;

    bool m_collisionFilterSet;
    int m_collisionFilterGroup;
    int m_collisionFilterMask;
};
}

#endif // RAYTEST_H
