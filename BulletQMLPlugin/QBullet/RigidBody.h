/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef RIGIDBODY_H
#define RIGIDBODY_H

#include <QObject>
#include <QSharedPointer>
#include "CollisionObject.h"

class btCollisionShape;
class btDiscreteDynamicsWorld;
class btRigidBody;
class btTransform;

namespace QBullet {

class CollisionShape;
class DiscreteDynamicsWorld;
class WorldData;

/*!
\class RigidBody RigidBody.h <QBullet/RigidBody.h>

Copied from: http://www.bulletphysics.org/mediawiki-1.5.8/index.php?title=Scaling_The_World

By default, Bullet assumes units to be in meters and time in seconds. Moving
objects are assumed to be in the range of 0.05 units, about the size of a
pebble, to 10, the size of a truck. The simulation steps in fraction of seconds
(1/60 sec or 60 hertz), and gravity in meters per square second (9.8 m/s^2).

If your objects are much bigger, and you use the default gravity, the
simulation will appear in slow-motion. Try to scale down the objects to around
1 in meter units first.

Scaling the world is when you choose to use a different set of units. For
example, using centimetres instead of metres. The effect is that Bullet deals
with much larger (or possibly smaller) floating point values during its
internal calculations.

Why Scale The World?

By scaling the world, you change dimensions and velocities
appropriately so that they are back within the range that Bullet was designed
for (0.05 to 10). Thus the simulation becomes more realistic.

Example Situations

Pool/Snooker Game

The balls are small, 4cm in diameter. In previous Bullet releases, many people
have found Bullet to be insufficiently precise and have had success by scaling
the world by a factor of 100, i.e. using centimetres instead of metres. Support
for objects of this size has improved in Bullet 2.74.

Outer Space

If you make values too large, you may run into problems with the accuracy of
the float itself, and also a SAP Broadphase may become prohibitively slow /
large. Also, Bullet might not work properly if you use a triangle mesh with
very large triangles. In this case you might consider using a unit like km, AU,
light years, or whatever is appropriate to keep the numbers sensible.

How To Scale The World

Obviously any collision shapes must be scaled appropriately, but there are also
some other things that need to be changed. In general, if you are scaling the
world by a factor of X then you must do the following:

Scale collision shapes about origin by X
Scale all positions by X
Scale all linear (but not angular) velocities by X
Scale linear [Sleep Threshold] by X
Scale gravity by X
Scale all impulses you supply by X
Scale all torques by X^2
Scale all inertias by X if not computed by Bullet --> ?? should'nt this be X^2,unit of moment_of_inertia is kg*m^2

Damping is a ratio so this does not need to be changed.

Angular velocity should not need to be changed either.

If you find other things that need scaling, please add them here.

You should not have to change the mass unless you are dealing with massive
spaceships or other situations where you start to stress the float
representation. If you do scale it by a factor of Y then you must additionally:
(untested)

Scale all impulses by Y
Scale all inertias by Y if not computed by Bullet
Scale all torques by Y
*/
class RigidBody : public CollisionObject
{
    Q_OBJECT

    Q_PROPERTY(qreal mass READ mass WRITE setMass NOTIFY massChanged)

    Q_PROPERTY(QVector3D gravity READ gravity WRITE setGravity NOTIFY gravityChanged)
    Q_PROPERTY(qreal linearDamping READ linearDamping WRITE setLinearDamping NOTIFY linearDampingChanged)
    Q_PROPERTY(qreal angularDamping READ angularDamping WRITE setAngularDamping NOTIFY angularDampingChanged)
    Q_PROPERTY(QVector3D linearVelocity READ linearVelocity WRITE setLinearVelocity NOTIFY linearVelocityChanged)
    Q_PROPERTY(QVector3D angularVelocity READ angularVelocity WRITE setAngularVelocity NOTIFY angularVelocityChanged)

    //Restitution 0~1 none~max
    Q_PROPERTY(qreal restitution READ restitution WRITE setRestitution NOTIFY restitutionChanged)

    //Friction 0~1 none~max
    Q_PROPERTY(QVector3D anisotropicFriction READ anisotropicFriction WRITE setAnisotropicFriction NOTIFY anisotropicFrictionChanged)
    Q_PROPERTY(qreal friction READ friction WRITE setFriction NOTIFY frictionChanged)
    Q_PROPERTY(qreal rollingFriction READ rollingFriction WRITE setRollingFriction NOTIFY rollingFrictionChanged)
    Q_PROPERTY(qreal spinningFriction READ spinningFriction WRITE setSpinningFriction NOTIFY spinningFrictionChanged)

public:
    explicit RigidBody(QObject *parent = nullptr);

    ~RigidBody();

    QSharedPointer<btRigidBody> body() const;

    qreal mass() const;

    QVector3D linearVelocity() const;

    QVector3D angularVelocity() const;

    QVector3D gravity() const;

    qreal restitution() const;

    qreal friction() const;

    qreal rollingFriction() const;

    qreal spinningFriction() const;

    QVector3D anisotropicFriction() const;

    qreal linearDamping() const;

    qreal angularDamping() const;

signals:
    void massChanged(qreal mass);

    void linearVelocityChanged(QVector3D linearVelocity);

    void angularVelocityChanged(QVector3D angularVelocity);

    void gravityChanged(QVector3D gravity);

    void restitutionChanged(qreal restitution);

    void frictionChanged(qreal friction);

    void rollingFrictionChanged(qreal rollingFriction);

    void anisotropicFrictionChanged(QVector3D anisotropicFriction);

    void linearDampingChanged(qreal linearDamping);

    void angularDampingChanged(qreal angularDamping);

    void spinningFrictionChanged(qreal spinningFriction);

public slots:
    void applyForce(const QVector3D &force, const QVector3D &localPoint);

    /*!
    With applyCentralForce you need to apply a force for a period of time to
    really have the effect of, for example, throwing (so it is usually not
    enough to only call it once).
    */
    void applyCentralForce(const QVector3D &force);

    /*!
    A convenient function to apply a certain force for a certain amount of time
    in one step.

    The applyCentralImpulse is more like setting a velocity directly to the
    rigid body.
    */
    void applyCentralImpulse(const QVector3D &force);

    void applyTorque(const QVector3D &torque);

    void applyTorqueImpulse(const QVector3D &torque);

    void clearForces();

    void setMass(qreal mass);

    void setLinearVelocity(QVector3D linearVelocity);

    void setAngularVelocity(QVector3D angularVelocity);

    void setGravity(QVector3D gravity);

    void setRestitution(qreal restitution);

    void setFriction(qreal friction);

    void setRollingFriction(qreal rollingFriction);

    void setSpinningFriction(qreal spinningFriction);

    void setAnisotropicFriction(QVector3D anisotropicFriction);

    void setLinearDamping(qreal linearDamping);

    void setAngularDamping(qreal angularDamping);

protected:
    void onAddToWorld(btDiscreteDynamicsWorld *world,
                      bool hasFilterSet,
                      int collisionFilterGroup,
                      int collisionFilterMask) override;
    void onWorldGravityChanged() override;
    void onCollisionShapePropertyChanged() override;
    void onCollisionShapeReset() override;
    void onWorldChanged() override;
    void postCreate() override;
    QSharedPointer<btCollisionObject> create() const override;
    void onOriginChanged(const QVector3D &origin) override;
    void onRotationChanged(const QQuaternion &rotation) override;

    /*!
    updateFromBulletEngine() is called from MotionState to update transform and
    any dynamic properties.
    */
    bool updateFromBulletEngine(const btTransform &mat) override;

private:
    friend class MotionState;
    void updateMassProps();

    QSharedPointer<btRigidBody> m_body;

    qreal m_mass;

    QVector3D m_linearVelocity, m_angularVelocity;
    bool m_gravitySet;
    QVector3D m_gravity;

    bool m_anisotropicFrictionSet;
    QVector3D m_anisotropicFriction;
    qreal m_linearDamping, m_angularDamping;

    qreal m_restitution, m_friction, m_rollingFriction, m_spinningFriction;

};

}

#endif // RIGIDBODY_H
