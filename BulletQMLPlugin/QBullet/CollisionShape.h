/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef COLLISIONSHAPE_H
#define COLLISIONSHAPE_H

#include <QSharedPointer>
#include <QVector3D>
#include "BulletObject.h"

class btCollisionShape;

namespace QBullet {

/*!
\class CollisionShape CollisionShape <QBullet/CollisionShape.h>

The CollisionShape class provides an interface for collision shapes that can be
shared among CollisionObjects.

Like graphical meshes, collision shapes allow one to collide a rich variety of
different objects that one might encounter in the real world. Collision shapes
don't have a world position, they are attached to collision objects or rigid
bodies.

Many rigid bodies can each have the same collision shape, which helps to save
memory. Unlike graphical meshes, collision shapes are not always composed of
triangles, but they can be represented as a primitive such as box, cylinder.
This page walks us through the various Bullet collision shapes and gives some
advice about when to use each kind.

Using convex collision shapes such as convex hulls and primitives such as a
sphere, box and cylinder makes the collision algorithms much more simple and
efficient. Collision shapes for dynamic rigid bodies should define a finite
volume. Collision shapes with a finite volume can calculate an inertia tensor
given a mass, which is needed for dynamic rigid bodies. Some collision shapes,
such as a plane shape or static triangle mesh don't have a finite volume or an
inertia tensor, so they should only be used with static rigid bodies.
Typically, one will define a large amount of static environment with
btBvhTriangleMeshShape and maybe also btHeightfieldTerrainShape, although any
of the other shapes can be used as well.

Dynamic rigid bodies will ideally be primitives or btConvexHull, but can also
be compositions of such shapes and thus dynamic concave rigid bodies, e.g.
aeroplanes and chairs are possible.

\see
http://www.bulletphysics.org/mediawiki-1.5.8/index.php/Collision_Shapes
*/
class CollisionShape  : public BulletObject
{
    Q_OBJECT
    Q_PROPERTY(QVector3D scale READ scale WRITE setScale NOTIFY scaleChanged)
    Q_PROPERTY(QString name READ name NOTIFY nameChanged)
public:
    virtual ~CollisionShape();

    QSharedPointer<btCollisionShape> shape() const;

    QVector3D scale() const;

    QString name() const;

signals:
    void scaleChanged(QVector3D scale);
    //Emit when the shape's property is changed.
    void propertyChanged();
    //Emit when the shape is replaced with a new shape.
    void shapeReset();

    void nameChanged(QString name);

public slots:
    void setScale(QVector3D scale);

protected slots:
    virtual void clear();

    void resetShape();
protected:
    virtual QSharedPointer<btCollisionShape> create() const = 0;

    virtual void postClear();

    virtual void preCreate();

    virtual void postCreate();

    void onComponentComplete() override;
    /*
    Need to call setPropertyChanged whenever shape's property is changed.

    It emits propertyChanged signal. When RigidBody receives this signal
    it will update its mass property and other related properties.
    */
    void setPropertyChanged();

    explicit CollisionShape(QObject *parent = nullptr);

private:
    QSharedPointer<btCollisionShape> m_btShape;

    QVector3D m_scale;
    bool m_scaleSet;
};

}

#endif // COLLISIONSHAPE_H
